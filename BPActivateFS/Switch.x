
#import "FSSwitchDataSource.h" //Necessary for all flipswitches
#import "FSSwitchPanel.h" //Necessary for all flipswitches
#import "notify.h" //So, we can let the device know, we changed a setting
#include <UIKit/UIKit.h> // For our alert view and pretty much everything else
#import "CustomIOS7AlertView.h" // For a custom alert view that isn't working...
#import "GUAAlertView.h" // For our draggable alert view
#define BPFacebookPlist @"/var/mobile/Library/Preferences/com.alfadesigns.bluePillsettings.plist"  //A variable for our bluePill settings file. 
#import <AudioToolbox/AudioToolbox.h> //For the button sound


//Methods
// [self dismissInSeconds:(float)arg1 alert:(GUAAlertView *)arg2]; 
// This method will dismiss 
@interface UIAlertView (BPActivateFBSectionFSSwitch)

- (id)initWithTitle:(id)arg1 message:(id)arg2 delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5;

@end

@interface CustomIOS7AlertView (BPActivateFBSectionFSSwitch)

- (id)initWithTitle:(id)arg1 message:(id)arg2 delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5;

@end

@interface BPActivateFBSectionFSSwitch : NSObject <FSSwitchDataSource>
@end
//Necessary to delegate FSSwitchDataSource

@implementation BPActivateFBSectionFSSwitch //Where all the magic happens
-(FSSwitchState)stateForSwitchIdentifier:(NSString *)switchIdentifier { //This is where we set the state of the flipswitch (on or off)
	NSDictionary* BPActivateFBSectionSettings = [NSDictionary dictionaryWithContentsOfFile:BPFacebookPlist]; //Lets read the read receipt file
	return [[BPActivateFBSectionSettings objectForKey:@"FActivate"] boolValue] ? FSSwitchStateOn : FSSwitchStateOff; //If read receipts are on, the switch is on and vise versa
}

- (void)selectedItemAtIndex:(NSInteger)index {
    NSLog(@"Item selected at index %ld.", (long)index);
}
	
- (void)dismissInSeconds:(double)arg1 alert:(GUAAlertView *)arg2{
	    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(arg1 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
	[arg2 dismiss];
	});
	
	};

- (void)BPActivateFBSectionOn {
    
/*     NSString *_content = @"BluePill (Facebook.app) Enabled: \n Any instance of Facebook has been closed. \n Please restart to view changes. \n EmberPlus (1.0)";
       UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"bluePill (Facebook.app):" message:_content delegate:self cancelButtonTitle:@"Clear" otherButtonTitles:nil];
    [_moreInfo show]; */
   // GUAAlertView *v = nil;
	GUAAlertView *v = [GUAAlertView alertViewWithTitle:@"BluePill (FB.app): ON"
                                           message:@"BluePill for Facebook has been activated. \n Any instance of Facebook has been closed. \n Slide Down To Dismiss Notification \n EmberPlus (1.0)"
                                       buttonTitle:@"Clearing in 3, 2, 1..."
                               buttonTouchedAction:^{
                                       NSLog(@"button touched");
                                   } dismissAction:^{
                                       NSLog(@"dismiss");
                                   }];

[v show];
[self dismissInSeconds:2.0 alert:v];
}

- (void)BPActivateFBSectionOff {
	 	GUAAlertView *v = [GUAAlertView alertViewWithTitle:@"BluePill (FB.app): OFF"
                                           message:@"BluePill for Facebook has been disabled. \n Any instance of Facebook has been closed. \n Slide Down To Dismiss Notification \n EmberPlus (1.0)"
                                       buttonTitle:@"Clearing in 3, 2, 1..."
                               buttonTouchedAction:^{
                                       NSLog(@"button touched");
                                   } dismissAction:^{
                                       NSLog(@"dismiss");
                                   }];
[v show];
[self dismissInSeconds:2.0 alert:v];

}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{

    if (buttonIndex == 1)
    {
	system("open net.whatsapp.WhatsApp");
    }else{
	[alertView dismiss];
	}}


-(void)applyState:(FSSwitchState)newState forSwitchIdentifier:(NSString *)switchIdentifier { //Let's change read receipts, based on if the user switched the switch
	NSMutableDictionary* BPActivateFBSectionSettings = [NSMutableDictionary dictionaryWithContentsOfFile:BPFacebookPlist]; //Let's get the files contents

	if(newState == FSSwitchStateIndeterminate) { //If switch is neither on or off (the user dun goofed)..
		return; //Don't do anything
	} else if(newState == FSSwitchStateOn) { //If toggled on..
		[BPActivateFBSectionSettings setObject:[NSNumber numberWithBool:YES] forKey:@"FActivate"]; //turn reciepts on!
			[self BPActivateFBSectionOn]; //Curtens Enabled Message
  	NSLog(@"EmberPlus FB++ StealthMode Activated");

	} else if(newState == FSSwitchStateOff) { //But if toggled off..
		[BPActivateFBSectionSettings setObject:[NSNumber numberWithBool:NO] forKey:@"FActivate"]; //turn reciepts off!
		[self BPActivateFBSectionOff]; //BPActivateFBSection Disabled Message
  	NSLog(@"EmberPlus FB++: StealthMode Deactivated");

	}
	[BPActivateFBSectionSettings writeToFile:BPFacebookPlist atomically:YES]; //let's change the actual file with the changes we made above
	notify_post("com.alfadesigns.bluePillsettings-prefsChanged"); //aye apple, we just changed yo shit!
	
	//Close Settings
	NSLog(@"EmberPlus FB++: Killing: Settings.app");
    system("killall -9 Preferences"); 
	//Close FB
	NSLog(@"BPActivateFBSection: Killing Facebook");
   system("killall Facebook"); 
	//Close WhatsApp
	// NSLog(@"BPActivateFBSection: Re-Opening WhatsApp");
	// system("open net.whatsapp.WhatsApp"); 
	//Reopen WhatsApp
	}

-(NSString *)titleForSwitchIdentifier:(NSString *)switchIdentifier { //the title that shows when you click the flipswitch
	return @"BluePill (Facebook.app)"; //ya bish
}
@end