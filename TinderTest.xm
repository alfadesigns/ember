#import "TinderTest.h"
#import "CustomIOS7AlertView.h"
#import "CHDraggableView.h"
#import "CHDraggableView+Avatar.h"
#import "THPinViewController/THPinViewController.h"
#import "JFMinimalNotification/JFMinimalNotification.h"
#import "PropertyFile.h"
// Testing remote commit via codeanywhere.io
static TNDRDataManager *_hookedDataManager;
static TNDRRecommendationViewController *_hookedRVC;
static TNDRSlidingPagedViewController *_hookedSlidingPagedViewController;
static TNDRSlidingPagedViewController *_secondSPV;
static TNDRChatViewController *_hookedTNDRChatViewController;
static TNDRUser *_hookedTNDRUser;
static NSFetchedResultsController *_hookedFetchedResultsController; // our hooked NSFetchedResultsController object. 
static UICollectionView *_hookedCollectionView; // our hooked UICollectionView object. 




// #define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
// #define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
// #define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )

//%hook TNDRAppDelegate
// -(bool)application:(id)arg1 didFinishLaunchingWithOptions:(id)arg2 {
	// [JDStatusBarNotification showWithStatus:@"CupidsArrow Activated"];
//	_hookedSlidingPagedViewController = MSHookIvar<TNDRSlidingPagedViewController *>(self, "_slidingPagedViewController");	//hook into _fetchedResultsController Class Instance
	// return %orig(arg1, arg2); 
// }
// %end
//DO CHAT VIEW MODIFICATIONS HERE
%hook TNDRChatViewController
- (void)viewDidLoad {
%orig; // Call the original method implementations (VERY IMPORTANT)

}
%end

%hook TNDRDataManager
-(bool)isUpdatingRecommendations { if(Ignition) {
	%log;
	bool result = %orig;
	NSLog(@"CupidLog isUpdatingMatches: %d", result);
	return result;}
	else{ return %orig; }
}
//+ (id)sharedInstance {  %log; %orig; }
/* 
- (void)setPopularLocationMerger:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig; }
- (void)setPreventNewMatchMomentQueries:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig; }
- (bool)preventNewMatchMomentQueries {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig; return r; }
- (void)setFailedChoiceSendTimer:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig; }
- (id)failedChoiceSendTimer {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setMomentLikesMergingOperationQueue:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)momentLikesMergingOperationQueue {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setMomentsMergingOperationQueue:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)momentsMergingOperationQueue {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setMqttManager:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)mqttManager {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setChangedMatchesFetchedResultsController:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)changedMatchesFetchedResultsController {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setMomentImagePrefetcher:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)momentImagePrefetcher {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setMomentPrefetchURLs:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)momentPrefetchURLs {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setUserPrefetchURLs:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)userPrefetchURLs {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setMomentsTimer:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)momentsTimer {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setUpdatesTimer:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)updatesTimer {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setRecommendationsTimer:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)recommendationsTimer {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setUpdatingInBackground:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)isUpdatingInBackground {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)setPreviousMatchCount:(int)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (int)previousMatchCount {  %log; NSLog(@"CupidsDataManager: "); int r = %orig;  return r; }
- (void)setHasUnviewedMatches:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)hasUnviewedMatches {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)setUpdatingMomentLikes:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)isUpdatingMomentLikes {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)setUpdatingMomentsFeed:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)isUpdatingMomentsFeed {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)setUpdatingMyMoments:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)isUpdatingMyMoments {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)setUpdatingMoments:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)isUpdatingMoments {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (bool)isUpdatingMatches {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)setUpdatingRecommendations:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)isUpdatingRecommendations {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)setChangedPreferences:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)hasChangedPreferences {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)setDelegate:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)delegate {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setCurrentChatID:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)currentChatID {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)setOpenedApplicationDate:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)openedApplicationDate {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)sendFailedRecommendationChoices {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)startFailedRecsSendTimer:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)setUpdatingMatches:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)clearStaleRecommendations:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)imagePrefetcher:(id)fp8 didFinishWithTotalCount:(unsigned int)fp12 skippedCount:(unsigned int)fp16 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)doMomentImagePrefetch {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)isPrefetched:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)queuePrefetchForMyMoment:(id)fp8 size:(int)fp12 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)doUserImagePrefetch {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)queuePrefetchForUser:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updateUnviewedCounts {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)controllerDidChangeContent:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)setupChangedFetchedResultsController {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)resetMoments:(id)fparg {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)mergeBlockedMatchUpdates:(id)fp8 inContext:(id)fp12 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)mergeMatchUpdates:(id)fp8 inContext:(id)fp12 {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (unsigned int)mergeRecommendationUpdates:(id)fp8 inContext:(id)fp12 {  %log; NSLog(@"CupidsDataManager: "); unsigned int r = %orig;  return r; }
- (void)retrieveMomentsForNewMatches:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)removeMomentLikesForBlockedMatchWithUserID:(id)fp8 inContext:(id)fp12 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (unsigned int)mergeMomentsUpdates:(id)fp8 inContext:(id)fp12 {  %log; NSLog(@"CupidsDataManager: "); unsigned int r = %orig;  return r; }
- (void)markFriendsWhoAreMatches {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)removeCorrespondingRecRelatedToFriendRequest:(id)fp8 inContext:(id)fp12 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)removeAllIncomingMatchRequestsRelatedToUser:(id)fp8 inContext:(id)fp12 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)mergeFriendRequestUpdates:(id)fp8 inContext:(id)fp12 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updatePopularLocationsWithCompletion:(id)fparg {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)popularLocationMerger {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)saveRetrievedMomentsToPersistentStore:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)saveRetrievedMomentLikesToPersistentStore:(id)fp8 shouldNotify:(bool)fp12 completion:(id)fparg {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)saveRetrievedMomentLikesToPersistentStore:(id)fp8 shouldNotify:(bool)fp12 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)momentsIDPrefixForCurrentLanguage {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (id)languageCodeForCurrentPreferredLanguage {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (bool)hasTutorialMomentsForCurrentLanguage {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)removeMomentsForUnfollowedMatches:(id)fp8 inContext:(id)fp12 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)removeMomentsForUnfollowedMatch:(id)fp8 completion:(id)fparg {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)removeTutorialMoments {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)tutorialMomentsForCurrentLanguage {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)retrieveTutorialMoments {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)latestMomentIDForMyMoments {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (id)oldestMomentIDForMyMoments {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (id)lastSeenMomentID {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; }
- (void)timestampMomentsAPILastCalledToNow {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)canCallMomentsAPISinceLastCalledTime:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)updateMomentLikes {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updateMyMomentsFromOldestMomentID {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updateMyMomentsCascadeThruLikesCall:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updateMomentsFeedCascadeThruAllMomentsCalls:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updateMyMoments {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updateMomentsFeed {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updateMoments {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)didConnectToMQTT {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)enableMomentsPolling:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)momentsPollingEnabled {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)didDisconnectFromBrokerUnintentionally {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)disconnectFromTinderMQTTBroker {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)isMQTTConnected {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (bool)isMQTTEnabled {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)connectToTinderMQTTBrokerAfterUpdates {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updateMatches {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)sendNotificationRecsDone:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)updateRecommendations {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)startRecommendationUpdateTimer {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)invalidateUpdateTimer {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)doDataUpdates:(bool)fp8 {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)setupMQTTClient {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (bool)newPersistentStoreFilePresent:(id)fp8 {  %log; NSLog(@"CupidsDataManager: "); bool r = %orig;  return r; }
- (void)setupMomentsMergingOperationQueue {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)setupPrefetch {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (void)setup {  %log; NSLog(@"CupidsDataManager: "); %orig;  }
- (id)init {  %log; NSLog(@"CupidsDataManager: "); id r = %orig;  return r; } */

- (void)setChangedPreferences:(bool)fp8 {  %log;  if(Ignition) {
	[JDStatusBarNotification showWithStatus:@"Settings Saved"  dismissAfter:2.0 styleName:JDStatusBarStyleWarning];
	[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite];

	%orig;  } else { %orig; }
	}
%end

%hook TNDRSlidingPagedViewController

-(void)viewWillAppear:(bool)arg1 {  if(Ignition) {
	%orig(arg1);
	[JDStatusBarNotification showWithStatus:@"EmberPlus" dismissAfter:2.0 styleName:JDStatusBarStyleError];
[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite];
	%orig;  } else { %orig; }
}
%end



// Project: TinderTest.xm : CupidsArrow -- "Prototype"
// Class:    TNDRRecommendationViewController
// Instance Variable: NSFetchedResultsController *fetchedResultsController
// Array:      fetchedObjects
// Method:   count
//This will retrieve the number of reccomendations fetched. (i.e. The total number of profiles you have left to swipe)
TNDRRecommendationViewController *_recommendationView; // Hooked TNDRRecommendationViewController
int fetchedCount;// Where we'll store number of fetched recommendations.
int storedCount = 0; // If fetchedObjects is suddenly greater than oldCount then reccomendations list was refreshed and updated. 


%hook TNDRNoRecommendationsView // When you've got no more profiles left to swipe.
-(void)layoutSubviews { // Once we run out of profiles 
 if(Ignition) {
	[JDStatusBarNotification showWithStatus:@"No Tinder cards yet. Check back soon." dismissAfter:2.0 styleName:JDStatusBarStyleError]; // Drop notification
	%orig;  } else { %orig; }

}
%end

%hook TNDRMomentStackViewController

- (id)cardStackView:(id)fp8 cardForIndex:(int)fp12 { 
	%log;
	id result = %orig;
	NSLog(@"CupidLog: cardStackView 1st arg value %@", fp8);
	NSLog(@"CupidLog cardStackView  cardForIndex: %d", fp12);
	return result;
}
int totalStack = 0;
- (int)numberOfCardsOnStack:(id)fp8{
 if(Ignition) {
	int result = %orig;
	int correctedResult = result-1;
	NSLog(@"CupidLog: numberOfCardsOnStack 1st arg value %@", fp8);
	if((totalStack !=0) && (result > totalStack)){
		[JDStatusBarNotification showWithStatus:[NSString stringWithFormat:@"List Updated: %d recommendations cards available!", correctedResult]];			
				[JDStatusBarNotification showProgress:0.0];

	}
	totalStack = %orig;
	return %orig;
}

else {
return %orig;
}
}
%end

%hook TNDRAdCardCellManager
-(bool)internal_isTinderPlusSubscriber {
	if(Ignition){ return TRUE; } else{ return %orig;} 
}

-(void)setInternal_isTinderPlusSubscriber:(bool)arg1 {
if(Ignition){ bool r = TRUE; %orig(r); } else{ %orig(arg1); } }

-(void)configureForTinderPlusSubscriptionStatus:(bool)arg1 {
if(Ignition) {
%orig(TRUE);
}
else{
%orig(arg1);
} }
%end

%hook TNDRPaywallGatekeeper
-(bool)isPaywallBeingShownToUser {
	if(Ignition){ return FALSE; } else{ return %orig; } 
}

-(void)setPaywallBeingShownToUser:(bool)arg1 {
if(Ignition){ bool r = FALSE; %orig(r); } else{ %orig(arg1); } }


-(bool)isTinderPlusActive { if(Ignition) {
return TRUE; } else{ return %orig; } }

%end //=====End TNDPayWallGatekeeper hook

%hook TNDRCurrentUser
-(bool)isTinderPlusSubscriber {
	if(Ignition){ return TRUE; } else{ return %orig;} 
}

-(bool)isTinderPlusEnabled {
	if(Ignition){ return TRUE; } else{ return %orig;} 
}
%end

/* %hook TNDRAnalyticsTracker

-(void)trackPassportChooseLocationWithLat:(double)arg1 andLong:(double)arg2 isExpired:(bool)arg3 isTinderPlusSubscriber:(bool)arg4 {
	if(Ignition){ bool r = FALSE; bool s = TRUE; %orig(arg1, arg2, r, s); } else{ %orig(arg1, arg2, arg3, arg4); }
}

-(void)trackTinderPlusRoadblockViewWithProductInfo:(id)arg1 otherID:(id)arg2 percentLikesLeft:(double)arg3  timeRemaining:(long long)arg4 {
	if(Ignition) {
		double r = 100.0;
		long long s = 0.0;
		%orig(arg1, arg2, r, s);
	}
	else{
		%orig(arg1, arg2, arg3, arg4);
	}
}

-(void)trackTinderPlusRoadblockCancelWithProductInfo:(id)arg1 otherID:(id)arg2 percentLikesLeft:(double)arg3  timeRemaining:(long long)arg4 {
	if(Ignition) { double r = 100.0; long long s = 0.0; %orig(arg1, arg2, r, s); } else{ %orig(arg1, arg2, arg3, arg4); } }

-(void)trackTinderPlusRoadblockSelectSku:(id)arg1 withPrice:(id)arg2 andLocale:(id)arg3  otherID:(long long)arg4 percentLikesLeft:(double)arg5  timeRemaining:(long long)arg6{
	if(Ignition) { double r = 100.0; long long s = 0.0; %orig(arg1, arg2, arg3, arg4, r, s); } else{ %orig(arg1, arg2, arg3, arg4, arg5, arg6); } }

-(void)trackTinderPlusPurchaseOption:(id)arg1 withPrice:(id)arg2 andLocale:(id)arg3 percentLikesLeft:(double)arg4 unlimitedLikesOffered:(bool)arg5 {
	if(Ignition) { double r = 100.0; bool s = TRUE; %orig(arg1, arg2, arg3, r, s); } else{ %orig(arg1, arg2, arg3, arg4, arg5); } }
	
-(void)trackTinderPlusPurchase:(id)arg1 withPrice:(id)arg2 andLocale:(double)arg3  from:(unsigned long long)arg4 percentLikesLeft:(double)arg5  unlimitedLikesOffered:(bool)arg6 {
	if(Ignition) { double r = 100.0; bool s = TRUE; %orig(arg1, arg2, arg3, arg4, r, s); } else{ %orig(arg1, arg2, arg3, arg4, arg5, arg6); } }

-(void)trackTinderPlusPaywallViewAllProducts:(id)arg1 encounteredFrom:(unsigned long long)arg2 percentLikesLeft:(double)arg3 unlimitedLikesOffered:(bool)arg4 {
	if(Ignition){ double r = 100.0; bool s = TRUE; %orig(arg1, arg2, r, s); } else{ %orig(arg1, arg2, arg3, arg4); } }
	
-(void)trackTinderPlusExitPaywallCancelWithProductInfo:(id)arg1	percentLikesLeft:(double)arg2 unlimitedLikesOffered:(bool)arg3 {
if(Ignition){ double r = 100.0; bool s = TRUE; %orig(arg1, r, s); } else{ %orig(arg1, arg2, arg3); } }

%end */

 


 /*
%hook TNDRRecommendationViewController
- (void)finishSwipingInDirection:(int)fp8 withVelocity:(struct CGPoint)fp12 {
 if(Ignition) {
	_hookedCollectionView = MSHookIvar<UICollectionView *>(self, "_collectionView");	//hook into _fetchedResultsController Class Instance
//	_hookedRVC = MSHookIvar<TNDRRecommendationViewController	 *>(self, "_hookedRVC");	//hook into _fetchedResultsController Class Instance
	if([_hookedCollectionView.visibleViews count] > storedCount)  	{ 
		fetchedCount = [_hookedCollectionView.visibleViews count]; // Extract count of fetched results
		NSLog(@"CupidCount:  fetchedCount  = %d  ============ 1", fetchedCount); //Log the fetchedCount total
		NSLog(@"CupidCount: _hookedCollectionView %lu", [_hookedCollectionView.visibleViews count] ); 	
		int correctedResults = fetchedCount-1; 
		NSLog(@"CupidCount: List Updated Sucessfully to %d ", (fetchedCount-1));  
		[JDStatusBarNotification showWithStatus:[NSString stringWithFormat:@"List was updated! %d cards now available to swipe!", correctedResults] styleName:JDStatusBarStyleSuccess];	}
	else{
		NSLog(@"CupidCount: List  Not Updated: Proceeding To Queue %d ", (fetchedCount-1));  
		fetchedCount = [_hookedCollectionView.visibleViews count]; // Extract count of fetched results
		int correctedResults = fetchedCount-1; 
		//Show The Notification When Swiping and No Update has Taken Place.
		
		[JDStatusBarNotification showWithStatus:[NSString stringWithFormat:@"You've got %d recommendations remaining in queue", correctedResults]]; //Banner Notify Count
		[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite];
			[JDStatusBarNotification showProgress:1.0];

			//
//notification.image = [UIImage imageWithContentsOfFile:@"/Library/Application Support/TNDRSelect/stack.png"];

	}
	storedCount = fetchedCount;
//	[self likeUserForTopCard];

	%orig;
	}
	else { %orig; }
	} 
	
	%end
	
// -(id)cellForTopCardOnStack { //Use to retrieve index for front-most user on stack. [NSArray count]
	// id originalLogic = %orig; // This is the original return value the method is expected to return. We still need it to do it's job. Whatever that may be.
	// _hookedFetchedResultsController = MSHookIvar<NSFetchedResultsController *>(self, "_fetchedResultsController");	//hook into _fetchedResultsController Class Instance
	// return originalLogic;
// }
	
	// %new 
	// - (UIView *)createDemoView
// {

// UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
// UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
// NSLog(@"CurtainsDemo: ");
// return demoView;
// }

//%end	 
*/

static void PreferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) { // Action when listener detects a change
    //Perform action when switching a preference specifier
	
    NSLog(@"Killing Tinder");
    system("killall Tinder"); //Close Tinder
    NSLog(@"Killing Settings.app");
    system("killall Settings"); //Close Settings

    
 NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //ceate pointer to peferences path
 
 Ignition = [preferences[PREFERENCES_ENABLED_YTIgnition_KEY] boolValue];// This switch defines Ignition as the preference boolean
 //Ignition points to the Activate switch in the settings bundle


  
}
%ctor { // runs first. This is what happens once the target is run. Prior to code-insertion. 
	NSLog(@"CupidsArrow:  Injecting Tinder.dylib");
	
	
   [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *block) { //add listener

	NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //Define preference path here. (PREFERENCES_PATH).
	
	if (preferences == nil) {
	    preferences = @{ 

		    PREFERENCES_ENABLED_YTIgnition_KEY : @(NO)  // No comma

			
			
			}; // Default preference value to no
	    
	    [preferences writeToFile:PREFERENCES_PATH atomically:YES];
	} else {

 Ignition = YES;
 

 Ignition = [preferences[PREFERENCES_ENABLED_YTIgnition_KEY] boolValue];
	    
		
			if(Ignition){ 
	NSLog(@"EmberPlus: EmberPlus Activated: Initializing");

	}
	else{
	NSLog(@"EmberPlus: EmberPlus Deactivated. Target will run unmodified.");
	}
	
	}
	CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, PreferencesChangedCallback, CFSTR(PREFERENCES_CHANGED_NOTIFICATION), NULL, CFNotificationSuspensionBehaviorCoalesce);
    }];
}