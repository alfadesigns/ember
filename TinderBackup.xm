#import "substrate.h"

/* ***************************************************
// File:= CupidsArrow.xm
//  By: Suhaib Alfaqeeh
// Target: com.cardify.tinder
//  Copyright 2011-2014, alfaDesigns - All rights reserved.
/****************************************************/

%hook TNDRSlidingPageViewController
-(void)viewDidAppear:(BOOL)arg1 { %log;
	NSLog(@"CupidsArrow:  CupidsArrow.dylib was injected succesfully!");
	%orig(arg1); //Run original method's logic.
}
%end

%hook TNDRSlidingPagedViewController
-(void)likeTapped:(id)arg1 { %log;
	%orig(arg1)	;
	NSLog(@"CupidsArrow:  Like Tapped");
}
%end

%hook TNDRSlidingPagedViewController
+ (id)viewControllerNavigationKey { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setStartPulseTime:(id)fp8 { %log; %orig; }

- (id)startPulseTime { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setChatViewController:(id)fp8 { %log; %orig; }

- (id)chatViewController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setViewControllerKeys:(id)fp8 { %log; %orig; }

- (id)viewControllerKeys { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setPresentingCameraMoments:(BOOL)fp8 { %log; %orig; }

- (BOOL)presentingCameraMoments { %log; BOOL r = %orig; NSLog(@"CupidsArrow:   = %d", r); return r;}

- (void)setCurNormalizedScrollOffsetX:(float)fp8 { %log; %orig; }

- (float)curNormalizedScrollOffsetX { %log; float r = %orig;; NSLog(@"CupidsArrow:   = %f", r); return r;}

- (void)setCurrentMomentShareTarget:(int)fp8 { %log; %orig; }

- (int)currentMomentShareTarget { %log; int r = %orig;; NSLog(@"CupidsArrow:   = %d", r); return r;}

- (void)setMatchWindow:(id)fp8 { %log; %orig; }

- (id)matchWindow { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setMatchesTitleButton:(id)fp8 { %log; %orig; }

- (id)matchesTitleButton { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setMomentsHeaderItem:(id)fp8 { %log; %orig; }

- (id)momentsHeaderItem { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setMatchToChatAnimationController:(id)fp8 { %log; %orig; }

- (id)matchToChatAnimationController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setMomentsActivityViewController:(id)fp8 { %log; %orig; }

- (id)momentsActivityViewController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setMomentsCameraViewController:(id)fp8 { %log; %orig; }

- (id)momentsCameraViewController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setMomentsFooter:(id)fp8 { %log; %orig; }

- (id)momentsFooter { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setForceMQTTDisconnectButton:(id)fp8 { %log; %orig; }

- (id)forceMQTTDisconnectButton { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setDefaultIndex:(int)fp8 { %log; %orig; }

- (int)defaultIndex { %log; int r = %orig;; NSLog(@"CupidsArrow:   = %d", r); return r;}

- (void)setRecommendationsViewController:(id)fp8 { %log; %orig; }

- (id)recommendationsViewController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setMatchesWithMomentsViewController:(id)fp8 { %log; %orig; }

- (id)matchesWithMomentsViewController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setContacter:(id)fp8 { %log; %orig; }

- (id)contacter { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setMenuViewController:(id)fp8 { %log; %orig; }

- (id)menuViewController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setMainSlidingNavbarSnapshot:(id)fp8 { %log; %orig; }

- (void)setCameraButton:(id)fp8 { %log; %orig; }

- (id)cameraButton { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

// - (void)transitionTo:(id)fp8 optionsDict:(id)fp12 animated:(BOOL)fp16 completion:(id)fpARG { %log; %orig; }

- (id)interactionControllerForDismissal:(id)fp8 { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (id)animationControllerForDismissedController:(id)fp8 { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (id)animationControllerForPresentedController:(id)fp8 presentingController:(id)fp12 sourceController:(id)fp16 { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)didTapChatCloseButton { %log; %orig; }

- (void)didBlockUser { %log; %orig; }

- (void)didRequestToViewRecommendations { %log; %orig; }

- (void)didRequestToEnableSearching:(BOOL)fp8 animated:(BOOL)fp12 { %log; %orig; }

// - (void)transitionToCameraMomentsCompletion:(id)fpARG { %log; %orig; }

- (void)presentMomentViewer:(id)fp8 { %log; %orig; }

- (void)momentViewerClosed:(id)fp8 { %log; %orig; }

- (struct CGRect)targetRectOfMomentShareAnimation { %log; struct CGRect r = %orig;; return r;}

- (void)performFinalSharingAnimation:(int)fp8 { %log; %orig; }

- (void)performSharingAnimationToTarget:(struct CGRect)fp8 { %log; %orig; }

- (id)setupAnimationWindowForShareWithImage:(id)fp8 { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)animateShareWithCompositedImage:(id)fp8 { %log; %orig; }

- (void)handleMomentViewerShareNotification:(id)fp8 { %log; %orig; }

- (void)didTapSettingsDoneButton { %log; %orig; }

- (void)scrollViewDidScroll:(id)fp8 { %log; %orig; }

- (void)preloadCameraMoments { %log; %orig; }

- (void)handleMomentsCameraButtonTapped:(id)fp8 { %log; %orig; }

- (void)shrinkMomentsCameraButtonForTesting { %log; %orig; }

- (void)setupMomentsFooter { %log; %orig; }

- (void)adjustMomentsFooterFrame:(float)fp8 { %log; %orig; }

- (void)handleStatusBarFrameChanged:(id)fp8 { %log; %orig; }

- (void)showAddFriendsControllerFromViewController:(id)fp8 { %log; %orig; }

- (void)handleSettingsItemTapped:(id)fp8 { %log; %orig; }

- (void)removePulseAnimationAfterWholePulse { %log; %orig; }

- (void)handlePullToRefreshReset:(id)fp8 { %log; %orig; }

- (void)handlePullToRefreshAmount:(id)fp8 { %log; %orig; }

- (void)handlePullToRefreshTriggered:(id)fp8 { %log; %orig; }

- (void)adjustTitleItemForPullDownAmount:(id)fp8 { %log; %orig; }

- (void)triggerRefreshForSource:(unsigned int)fp8 { %log; %orig; }

- (void)pulseTitleItemForLoading:(unsigned int)fp8 { %log; %orig; }

- (id)buildNewSettingsViewController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)preloadViewControllers { %log; %orig; }

- (void)presentChatForMatch:(id)fp8 withAvatarImage:(id)fp12 completion:(id)arg3 { %log; %orig; }

- (id)momentViewerFromNotification:(id)fp8 { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)handleFriendsEnabledChange:(id)fp8 { %log; %orig; }

- (void)handleChatBadgeNotification:(id)fp8 { %log; %orig; }

- (void)handleMyMomentsActivityShowBadgeNotification:(id)fp8 { %log; %orig; }

- (void)handleMyMomentsActivityHideBadgeNotification:(id)fp8 { %log; %orig; }

- (void)handleRequestedOpenChatNotification:(id)fp8 { %log; %orig; }

- (void)handleMomentViewerDismissedNotification:(id)fp8 { %log; %orig; }

- (void)handlePresentMomentViewerNotification:(id)fp8 { %log; %orig; }

- (void)handleHideMomentsFooter:(id)fp8 { %log; %orig; }

- (void)handleShowMomentsFooter:(id)fp8 { %log; %orig; }

- (void)handleMomentsFooterVisibility:(id)fp8 { %log; %orig; }

- (void)handleSlidingPagedViewControllerDidChange:(id)fp8 { %log; %orig; }

- (void)handleSlidingPagedViewControllerWillChange:(id)fp8 { %log; %orig; }

- (void)handleStatusBarChangeRequest:(id)fp8 { %log; %orig; }

- (void)handleNavigateToMyMomentsView:(id)fp8 { %log; %orig; }

- (void)handleNavigateToMatchesView:(id)fp8 { %log; %orig; }

- (void)handleNavigateToGameView:(id)fp8 { %log; %orig; }

- (void)handleUserDidLogout:(id)fp8 { %log; %orig; }

- (void)handleAppWillEnterForeground:(id)fp8 { %log; %orig; }

- (void)handleAppDidEnterBackground:(id)fp8 { %log; %orig; }

- (void)registerNotifications { %log; %orig; }

- (id)mainSlidingNavbarSnapshot { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)showStatusBarIfPossible { %log; %orig; }

- (void)viewDidAppear:(BOOL)fp8 { %log; %orig; }

- (void)viewWillAppear:(BOOL)fp8 { %log; %orig; }

- (id)buildDefaultTinderViewControllers { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (id)initWithDefaultTinderSetup { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (id)init { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

%end

%hook TNDRRecommendationViewController
+ (id)viewControllerNavigationKey { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setOpeningUserProfile:(BOOL)fp8 { %log; %orig; }

- (BOOL)openingUserProfile { %log; BOOL r = %orig; NSLog(@"CupidsArrow:   = %d", r); return r;}

- (void)setRecProfilePreviewViewController:(id)fp8 { %log; %orig; }

- (id)recProfilePreviewViewController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setTrackSparksRecsStartTimer:(id)fp8 { %log; %orig; }

- (id)trackSparksRecsStartTimer { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setGameToNewMatchTransitioning:(id)fp8 { %log; %orig; }

- (id)gameToNewMatchTransitioning { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setGameToProfileTransitioning:(id)fp8 { %log; %orig; }

- (id)gameToProfileTransitioning { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setPassStampView:(id)fp8 { %log; %orig; }

- (id)passStampView { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setLikeStampView:(id)fp8 { %log; %orig; }

- (id)likeStampView { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setSnapshotContainerView:(id)fp8 { %log; %orig; }

- (id)snapshotContainerView { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setUserInteractionEnabled:(BOOL)fp8 { %log; %orig; }

- (BOOL)userInteractionEnabled { %log; BOOL r = %orig; NSLog(@"CupidsArrow:   = %d", r); return r;}

- (void)setDiscoverOffView:(id)fp8 { %log; %orig; }

- (id)discoverOffView { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setNoRecommendationsView:(id)fp8 { %log; %orig; }

- (id)noRecommendationsView { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setCardSnapshotShadow:(id)fp8 { %log; %orig; }

- (id)cardSnapshotShadow { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setCurrentCardSnapshot:(id)fp8 { %log; %orig; }

- (id)currentCardSnapshot { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setTapGestureRecognizer:(id)fp8 { %log; %orig; }

- (id)tapGestureRecognizer { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setPanGestureRecognizer:(id)fp8 { %log; %orig; }

- (id)panGestureRecognizer { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setCollectionView:(id)fp8 { %log; %orig; }

- (id)collectionView { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setEmptyStackLayout:(id)fp8 { %log; %orig; }

- (id)emptyStackLayout { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setStackLayout:(id)fp8 { %log; %orig; }

- (id)stackLayout { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setFetchedObjectChanges:(id)fp8 { %log; %orig; }

- (id)fetchedObjectChanges { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setFetchedResultsController:(id)fp8 { %log; %orig; }

- (id)fetchedResultsController { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setButtonContainer:(id)fp8 { %log; %orig; }

- (id)buttonContainer { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setInfoButton:(id)fp8 { %log; %orig; }

- (id)infoButton { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setPassButton:(id)fp8 { %log; %orig; }

- (id)passButton { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setLikeButton:(id)fp8 { %log; %orig; }

- (id)likeButton { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)transitionTo:(id)fp8 optionsDict:(id)fp12 animated:(BOOL)fp16 completion:(id)fpARG { %log; %orig; }

- (id)animationControllerForDismissedController:(id)fp8 { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (id)animationControllerForPresentedController:(id)fp8 presentingController:(id)fp12 sourceController:(id)fp16 { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)reloadCellsToEnsureAllZIndicesAreLegit { %log; %orig; }

- (void)trackSparksRecsEnd { %log; %orig; }

- (void)trackSparksRecsStart { %log; %orig; }

- (void)updateDiscoverEnabled { %log; %orig; }

- (void)discoverButtonTapped { %log; %orig; }

- (void)removeDiscoverOffView { %log; %orig; }

- (void)showDiscoverOffiew { %log; %orig; }

- (void)removeNoRecommendationsView { %log; %orig; }

- (void)showNoRecommendationsView { %log; %orig; }

- (void)showProfileForTopCard { %log; %orig; }

- (void)presentNewMatch:(id)fp8 { %log; %orig; }

- (void)requestRecommendationsIfNecessary { %log; %orig; }

- (void)handleStatusBarChangedNotification:(id)fp8 { %log; %orig; }

- (void)handleRecommendationsUpdatedNotification:(id)fp8 { %log; %orig; }

- (void)handleNoRecommendationsNotification:(id)fp8 { %log; %orig; }

- (void)registerNotifications { %log; %orig; }

- (void)addFriendsTapped:(id)fp8 { %log; %orig; }

- (void)infoButtonTapped:(id)fp8 { %log; %orig; }

- (void)passTapped:(id)fp8 { %log; %orig; }

- (void)likeTapped:(id)fp8 { %log; %orig; }

- (void)passUserForTopCard { %log; %orig; }

- (void)likeUserForTopCard { %log; %orig; }

- (void)topCardTapped:(id)fp8 { %log; %orig; }

- (void)adjustCardsForPanningPosition:(float)fp8 { %log; %orig; }

- (void)adjustCardAtIndex:(int)fp8 horizontalTranslation:(float)fp12 { %log; %orig; }

- (void)passSwiped { %log; %orig; }

- (void)likeSwiped { %log; %orig; }

- (void)createAndShowSnapshotForCard:(id)fp8 { %log; %orig; }

- (void)highlightActionIndicators:(float)fp8 { %log; %orig; }

- (void)trackSwipeOnUser:(id)fp8 direction:(int)fp12 { %log; %orig; }

- (void)showAlertForFirstSwipeInDirection:(int)fp8 completion:(id)fpARG { %log; %orig; }

- (void)restoreViewToCenter:(id)fp8 { %log; %orig; }

- (void)translateViewOffscreen:(id)fp8 withVelocity:(struct CGPoint)fp12 direction:(int)fp20 { %log; %orig; }

- (void)restoreButtonHighlightColorToDefault { %log; %orig; }

- (void)finishSwipingInDirection:(int)fp8 withVelocity:(struct CGPoint)fp12 { %log; %orig; }

- (void)handlePanGesture:(id)fp8 { %log; %orig; }

- (BOOL)gestureRecognizerShouldBegin:(id)fp8 { %log; BOOL r = %orig; NSLog(@"CupidsArrow:   = %d", r); return r;}

- (id)gamePanGestureRecognizer { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)animateCardForDirection:(int)fp8 { %log; %orig; }

- (void)didTapProfileDoneButton { %log; %orig; }

- (void)didTapProfilePassButton { %log; %orig; }

- (void)didTapProfileLikeButton { %log; %orig; }

- (void)controllerDidChangeContent:(id)fp8 { %log; %orig; }

- (void)controller:(id)fp8 didChangeObject:(id)fp12 atIndexPath:(id)fp16 forChangeType:(unsigned int)fp20 newIndexPath:(id)fp24 { %log; %orig; }

- (unsigned int)numberOfRecommendationsInSection:(int)fp8 { %log; unsigned int r = %orig; NSLog(@"CupidsArrow:   = %u", r); return r;}

- (id)collectionView:(id)fp8 cellForItemAtIndexPath:(id)fp12 { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (int)collectionView:(id)fp8 numberOfItemsInSection:(int)fp12 { %log; int r = %orig; NSLog(@"CupidsArrow:   = %d", r); return r;}

- (int)numberOfSectionsInCollectionView:(id)fp8 { %log; int r = %orig; NSLog(@"CupidsArrow:   = %d", r); return r;}

- (id)cellForIndexOnStack:(unsigned int)fp8 { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (id)cellForTopCardOnStack { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (id)userForTopCardOnStack { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)setupButtonFrames { %log; %orig; }

- (void)setupGameCardFrames { %log; %orig; }

- (void)setupGestureRecognizer { %log; %orig; }

- (void)setupButtons { %log; %orig; }

- (void)setupSnapshotContainer { %log; %orig; }

- (void)setupDataSource { %log; %orig; }

- (void)setup { %log; %orig; }

- (void)viewDidLayoutSubviews { %log; %orig; }

- (void)viewDidDisappear:(BOOL)fp8 { %log; %orig; }

- (void)viewWillDisappear:(BOOL)fp8 { %log; %orig; }

- (void)viewDidAppear:(BOOL)fp8 { %log; %orig; }

- (void)viewWillAppear:(BOOL)fp8 { %log; %orig; }

- (void)viewDidLoad { %log; %orig; }

- (id)init { %log; id r = %orig; NSLog(@"CupidsArrow:   = %@", r); return r;}

- (void)dealloc { %log; %orig; }

- (void)didReceiveMemoryWarning { %log; %orig; }

%end

%ctor { // runs first. This is what happens once the target (net.whatsapp.WhatsApp) is run. Prior to code-insertion. 
NSLog(@"CupidsArrow:  Injecting Tinder.dylib");
}