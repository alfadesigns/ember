#import "FSSwitchDataSource.h" //Necessary for all flipswitches
#import "FSSwitchPanel.h" //Necessary for all flipswitches
#import "notify.h" //So, we can let the device know, we changed a setting
#include <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#define UAEnableStealthModePlist @"/Library/Application Support/FacebookPP/DefaultPreferences.plist" 
//So we can easily reference this file, let's make it a variable
// com.alfadesigns.UAEnableStealthMode_1.1-28_iphoneos-arm 
// DEB submitted to repo. 

@interface UIAlertView (UAEnableStealthModeFSSwitch)

- (id)initWithTitle:(id)arg1 message:(id)arg2 delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5;

@end

@interface CustomIOS7AlertView (UAEnableStealthModeFSSwitch)

- (id)initWithTitle:(id)arg1 message:(id)arg2 delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5;

@end

@interface UAEnableStealthModeFSSwitch : NSObject <FSSwitchDataSource>
@end
//Necessary to delegate FSSwitchDataSource

@implementation UAEnableStealthModeFSSwitch //Where all the magic happens
-(FSSwitchState)stateForSwitchIdentifier:(NSString *)switchIdentifier { //This is where we set the state of the flipswitch (on or off)
	NSDictionary* UAEnableStealthModeSettings = [NSDictionary dictionaryWithContentsOfFile:UAEnableStealthModePlist]; //Lets read the read receipt file
	return [[UAEnableStealthModeSettings objectForKey:@"UAEnableStealthMode"] boolValue] ? FSSwitchStateOn : FSSwitchStateOff; //If read receipts are on, the switch is on and vise versa
}

- (void)selectedItemAtIndex:(NSInteger)index {
    NSLog(@"Item selected at index %ld.", (long)index);
}

// - (void)UAEnableStealthModeOn {
    
    // NSString *_content = @"UAEnableStealthMode Enabled: \n Any instance of WhatsApp has been closed. \n \v1.1-8";
    
    // UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"UAEnableStealthMode:" message:_content delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Laucnh App", nil];
    // [_moreInfo show];
    
// }

// - (void)UAEnableStealthModeOff {
    
    // NSString *_content = @"UAEnableStealthMode Disabled: \n Any instance of WhatsApp has been closed. \n \n v1.1-8";
    
    // UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"UAEnableStealthMode:" message:_content delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Launch App", nil];
    // [_moreInfo show];
    
// }

- (void)UAEnableStealthModeOn {
    
    NSString *_content = @"StealthMode Enabled: \n Any instance of Facebook has been closed. \n Please restart to view changes. \n EmberPlus (1.0)";
       UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"FB++ Stealth Mode:" message:_content delegate:self cancelButtonTitle:@"Clear" otherButtonTitles:nil];
    [_moreInfo show];
    
}

- (void)UAEnableStealthModeOff {
    
     NSString *_content = @" StealthMode Disabled: \n Any instance of Facebook has been closed. \n EmberPlus (1.0)";
     UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"FB++ Stealth Mode:" message:_content delegate:self cancelButtonTitle:@"Clear" otherButtonTitles:nil];
     [_moreInfo show];
	
	
	//Begin Custom Alert View
	   //     UIImageView *iv = [[UIImageView alloc] initWithImage:nil];
		//[iv setImage:[UIImage imageNamed:@"ghost"]];
   //     CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
      //  [alertView setContainerView:iv];
     //   [alertView show];
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{

    if (buttonIndex == 1)
    {
	system("open net.whatsapp.WhatsApp");
    }else{
	[alertView dismiss];
	}
	}
	
	


-(void)applyState:(FSSwitchState)newState forSwitchIdentifier:(NSString *)switchIdentifier { //Let's change read receipts, based on if the user switched the switch
	NSMutableDictionary* UAEnableStealthModeSettings = [NSMutableDictionary dictionaryWithContentsOfFile:UAEnableStealthModePlist]; //Let's get the files contents

	if(newState == FSSwitchStateIndeterminate) { //If switch is neither on or off (the user dun goofed)..
		return; //Don't do anything
	} else if(newState == FSSwitchStateOn) { //If toggled on..
		[UAEnableStealthModeSettings setObject:[NSNumber numberWithBool:YES] forKey:@"UAEnableStealthMode"]; //turn reciepts on!
			[self UAEnableStealthModeOn]; //Curtens Enabled Message
  	NSLog(@"EmberPlus FB++ StealthMode Activated");

	} else if(newState == FSSwitchStateOff) { //But if toggled off..
		[UAEnableStealthModeSettings setObject:[NSNumber numberWithBool:NO] forKey:@"UAEnableStealthMode"]; //turn reciepts off!
		[self UAEnableStealthModeOff]; //UAEnableStealthMode Disabled Message
  	NSLog(@"EmberPlus FB++: StealthMode Deactivated");

	}
	[UAEnableStealthModeSettings writeToFile:UAEnableStealthModePlist atomically:YES]; //let's change the actual file with the changes we made above
	notify_post("com.unlimapps.fbpp-prefsChanged"); //aye apple, we just changed yo shit!
	
	//Close Settings
	NSLog(@"EmberPlus FB++: Killing: Settings.app");
    system("killall -9 Preferences"); 
	//Close Settings
	NSLog(@"UAEnableStealthMode: Killing Facebook");
   system("killall Facebook"); 
	//Close WhatsApp
	// NSLog(@"UAEnableStealthMode: Re-Opening WhatsApp");
	// system("open net.whatsapp.WhatsApp"); 
	//Reopen WhatsApp


	}

-(NSString *)titleForSwitchIdentifier:(NSString *)switchIdentifier { //the title that shows when you click the flipswitch
	return @"FB++ StealthMode"; //ya bish
}
@end