#include <substrate.h>
#include <UIKit/UIKit.h>
#import "EmberPlusPrefsListController.h"
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>
#import <sys/utsname.h>
#import "MobileGestalt.h"
#import "UILabel-AutomaticWriting/UILabel+AutomaticWriting.h"
#define STATUS_PATH @"/var/lib/dpkg/status"
#define DPKGL_PATH @"/var/tmp/dpkgl.log"

//#import "ViewController.h"

//===================
//ADMOB ADS
//====================

//=================================END ADMOB================


/* @interface ViewController (EmberPlus)

@end */

@interface UIImage (EmberPlus)

+ (id)imageNamed:(id)arg1 inBundle:(id)arg2;

@end

@interface UIAlertView (EmberPlus)

- (id)initWithTitle:(id)arg1 message:(id)arg2 delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5;

@end

@interface UITableViewCell (EmberPlus)

- (void)setSeparatorColor:(id)arg1;

@end


@interface PSTableCell : UITableViewCell
@end

@implementation EmberPlusPrefsListController
//blurPaper
/* - (void)showMenu {
    NSArray *items = [[NSArray alloc] initWithObjects:@"First Item", @"Second Item", @"Third Item", @"Fourth Item", nil];
    BlurMenu *menu = [[BlurMenu alloc] initWithItems:items parentView:self.view delegate:self];
    [menu show];
}

- (void)changeBackgroundImage {
    NSUInteger random = arc4random_uniform(3) + 1;
    NSString *backgroundImage = [NSString stringWithFormat:@"%lu.jpg", (unsigned long)random];
    /elf.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:backgroundImage]];
}


- (void)selectedItemAtIndex:(NSInteger)index {
    NSLog(@"Item selected at index %ld.", (long)index);
}

- (void)menuDidShow {
    NSLog(@"Menu appeared.");
}

- (void)menuDidHide {
    NSLog(@"Menu disappeared.");


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 */
//end blurPaper

- (instancetype)init {
    self = [super init];

    if (self) {
	
	UIImage *heartNorm = [UIImage imageNamed:@"flame.png" inBundle:[NSBundle bundleForClass:self.class]];
	UIImage *heartSelected = [UIImage imageNamed:@"flameOpen.png" inBundle:[NSBundle bundleForClass:self.class]];
	UIImage *infoNorm = [UIImage imageNamed:@"info.png" inBundle:[NSBundle bundleForClass:self.class]];
	UIImage *infoSelected = [UIImage imageNamed:@"info_filled.png" inBundle:[NSBundle bundleForClass:self.class]];
	
	UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[shareButton setFrame:CGRectMake(0, 0, heartNorm.size.width, heartNorm.size.height)];
	[shareButton setImage:heartNorm forState:UIControlStateNormal];
	[shareButton setImage:heartSelected forState:UIControlStateHighlighted];
	[shareButton addTarget:self action:@selector(tweetMe) forControlEvents:UIControlEventTouchUpInside];
	
	UIButton *moreInfoButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[moreInfoButton setFrame:CGRectMake(0, 0, infoNorm.size.width, infoNorm.size.height)];
	[moreInfoButton setImage:infoNorm forState:UIControlStateNormal];
	[moreInfoButton setImage:infoSelected forState:UIControlStateHighlighted];
	[moreInfoButton addTarget:self action:@selector(moreInfo) forControlEvents:UIControlEventTouchUpInside];
	//[moreInfoButton addTarget:self action:@selector(moreInfo) forControlEvents:UIControlEventTouchUpInside];

	
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:moreInfoButton] autorelease];
	self.navigationItem.titleView = shareButton;

    }

    return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
}

- (void)loadView {
    [super loadView];
		//A little theming
		[UISwitch appearanceWhenContainedIn:self.class, nil].onTintColor = [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1];/*#e74c3c*/
	    [UILabel appearanceWhenContainedIn:self.class, nil].textColor = [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1];/*#e74c3c*/
		[[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:1 green:0 blue:0.4 alpha:1]];/*#e74c3c*/
		self.view.tintColor = [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1];/*#e74c3c*/
		self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1]};
		self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1];/*#e74c3c*/ 		
		[[UIApplication sharedApplication] setStatusBarHidden:YES];

}                      

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
	
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    
    self.view.tintColor = nil;
    self.navigationController.navigationBar.tintColor = nil;
    self.navigationController.navigationBar.titleTextAttributes = @{};
	[[UIApplication sharedApplication] setStatusBarHidden:NO];
}




- (id)specifiers {
	if(_specifiers == nil) {
		_specifiers = [[self loadSpecifiersFromPlistName:@"EmberPlusPrefs" target:self] retain];
	}
	return _specifiers;
}

- (void)tweetMe {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
    
	SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
	[tweetSheet setInitialText:@"I love enhancing Tinder for iOS with EmberPlus, check it out in #Cydia. By: @alfadesignsco http://bit.ly/EmberPlus4Tinder"];
			[tweetSheet addImage:[UIImage imageNamed:@"EmberPlusNoTextFlameLogo@3x.png"]];

	[self presentViewController:tweetSheet animated:YES completion:nil];
    }
}
//============================================================
//===Add custom Methods here
//===============================
//-(void)killPrefs {
//	system("killall -9 Settings");
//	}
-(void)respring {
	system("killall -9 SpringBoard");
}

-(void)killSkype {
	system("killall -9 Skype");
}

-(void)killMessenger {
	system("killall -9 Messenger");
}

-(void)killPaper {
	system("killall -9 Paper");
}

- (void)twitIt
{
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tweetbot:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tweetbot:///user_profile/alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitterrific:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitterrific:///profile?screen_name=alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=alfadesignsco"]];
	} else {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com/alfadesignsco"]];
	}
}


- (void)goToReddit
{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://bit.ly/EmberPlus4iOSOnReddit"]];
}

- (void)goToBluePillLink
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"cydia://package/org.thebigboss.bluepill"]];
	}
	else{	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://bit.ly/bluepill4iOS"]]; }
}

- (void)fL_Paypal
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6BNJTR4JLFSE2"]];
}
//===============================

- (void)moreInfo {
    
    NSString *_content = @"EmberPlus: \n A Real-Time, Card-Counting, Monitor for Tinder. \n \n Icons by Icons8 \n www.icons8.com \n Contact me via Twitter or link below for questions, comments, or support. \n \n Developed by @alfaDesignsCo \n EmberPlus v1.0";
    
    UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"About" message:_content delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Support", nil];
    [_moreInfo show];
    
}

static NSString* machineName() {
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine
			      encoding:NSUTF8StringEncoding];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
	
	struct utsname systemInfo;
/*	uname(&systemInfo);
	NSString *deviceType = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]; // credit to OhhMee and others
	
	NSString *emailTitle = @"Support: EmberPlus (v1.0)";
	NSString *deviceInfo = [NSString stringWithFormat:@"%@-%@", deviceType, [[UIDevice currentDevice] systemVersion]];
	NSString *_fileName;
	NSData *fileData;
	
	MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
	mailCont.mailComposeDelegate = self;
	[mailCont setSubject:emailTitle];
	[mailCont setMessageBody:deviceInfo isHTML:NO];
	[mailCont setToRecipients:[NSArray arrayWithObject:@"alfadesigns.co@gmail.com"]];
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:DPKGL_PATH]) {
	    fileData = [NSData dataWithContentsOfFile:DPKGL_PATH];
	    _fileName = @"dpkgl.log";
	} else {
	    fileData = [NSData dataWithContentsOfFile:STATUS_PATH];  // probably shooting myself in the foot here :/
	    _fileName = @"status";
	}
	
	[mailCont addAttachmentData:fileData mimeType:@"text/plain" fileName:_fileName];
	[self presentViewController:mailCont animated:YES completion:NULL]; */
	uname(&systemInfo);
	NSString *deviceType = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]; // credit to OhhMee and others
	
	NSString *emailTitle = @"Support: EmberPlus (v1.0)";
	NSString *deviceInfo = [NSString stringWithFormat:@"%@-%@", deviceType, [[UIDevice currentDevice] systemVersion]];
	NSString *_fileName;
	NSData *fileData;
		MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] init];
	composeViewController.mailComposeDelegate = self;
	[composeViewController setToRecipients:[NSArray arrayWithObject:@"alfadesigns.co@gmail.com"]];
	[composeViewController setSubject:emailTitle];
	[composeViewController setMessageBody:[NSString stringWithFormat:@"\n\n\n-----\n%@\n%@", [[UIDevice currentDevice] systemVersion], machineName()] isHTML:NO];
	  [composeViewController addAttachmentData:[NSData dataWithContentsOfFile:@"/tmp/cydia.log"] mimeType:@"text/plain" fileName:@"cydia.log"];
	
	system("/usr/bin/dpkg -l >/tmp/dpkgl.log");
	[composeViewController addAttachmentData:[NSData dataWithContentsOfFile:@"/tmp/dpkgl.log"] mimeType:@"text/plain" fileName:@"dpkgl.log"];
  
	[self presentViewController:composeViewController animated:YES completion:nil];
	
    }	
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(int)result error:(NSError *)error{
    [self dismissModalViewControllerAnimated:YES];
}

@end

@interface EmberPlus_Header : PSTableCell
{
    UIView *headerView;
    UILabel *EmberPlusHeaderLabel;
    UILabel *EmberPlusHeaderSubLabel;
  //  UIButton *EmberPlus_LogoButton;
    UIButton *EmberPlus_TwitterButton;
    UIButton *EmberPlus_PaypalButton;
		@public
	UILabel *randLabel;
}
@end


@implementation EmberPlus_Header



- (void)setFrame:(struct CGRect)arg1 {
    
    [super setFrame:CGRectMake(0, 20, self.frame.size.width, 130)];
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	if((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])){
	int width = [[UIScreen mainScreen] bounds].size.width;

	CGRect randFrame = CGRectMake(0, 35, width, 60);
	self.backgroundColor = [UIColor clearColor];
	
	headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 130)];
	
/*	NSString *stringColor = @"#1C5276";
NSUInteger red, green, blue;
sscanf([stringColor UTF8String], "#%02X%02X%02X", &red, &green, &blue); 
UIColor *someColor = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1];
*/
/* 	EmberPlus_LogoButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[EmberPlus_LogoButton setFrame:CGRectMake(-21, 8, self.frame.size.width/2, 40)];
	[EmberPlus_LogoButton setImage:[UIImage imageNamed:@"button.png" inBundle:[NSBundle bundleForClass:self.class]] forState:UIControlStateNormal];
	[EmberPlus_LogoButton addTarget:self action:@selector(tindah) forControlEvents:UIControlEventTouchUpInside];
	[EmberPlus_LogoButton setBackgroundColor:[UIColor clearColor]];
   */
	EmberPlusHeaderLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 65)] autorelease];
	[EmberPlusHeaderLabel setText:@"Ember+" automaticWritingAnimationWithBlinkingMode:UILabelAWBlinkingModeWhenFinishShowing];
	[EmberPlusHeaderLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:55]];
	[EmberPlusHeaderLabel setTextAlignment:NSTextAlignmentCenter];
	[EmberPlusHeaderLabel setTextColor:[UIColor colorWithRed:1 green:0 blue:0.4 alpha:1]];/*#e74c3c*/
	[EmberPlusHeaderLabel setBackgroundColor:[UIColor clearColor]];
	
	EmberPlusHeaderSubLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 70, self.frame.size.width, 25)] autorelease];
	[EmberPlusHeaderSubLabel setText:@"© 2014-2015, alfaDesigns"];
	[EmberPlusHeaderSubLabel setFont:[UIFont systemFontOfSize:14]];
	[EmberPlusHeaderSubLabel setTextAlignment:NSTextAlignmentCenter];
	[EmberPlusHeaderSubLabel setTextColor:[UIColor grayColor]];
	[EmberPlusHeaderSubLabel setBackgroundColor:[UIColor clearColor]];

	EmberPlus_TwitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[EmberPlus_TwitterButton setFrame:CGRectMake(0, 90, self.frame.size.width/2, 40)];
	[EmberPlus_TwitterButton setImage:[UIImage imageNamed:@"twitterLogo.png" inBundle:[NSBundle bundleForClass:self.class]] forState:UIControlStateNormal];
	[EmberPlus_TwitterButton addTarget:self action:@selector(fL_Twitter) forControlEvents:UIControlEventTouchUpInside];
	[EmberPlus_TwitterButton setBackgroundColor:[UIColor clearColor]];

	EmberPlus_PaypalButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[EmberPlus_PaypalButton setFrame:CGRectMake(self.frame.size.width/2, 90, self.frame.size.width/2, 40)];
	[EmberPlus_PaypalButton setImage:[UIImage imageNamed:@"PayPalIcon.png" inBundle:[NSBundle bundleForClass:self.class]] forState:UIControlStateNormal];
	[EmberPlus_PaypalButton addTarget:self action:@selector(fL_Paypal) forControlEvents:UIControlEventTouchUpInside];
	[EmberPlus_PaypalButton setBackgroundColor:[UIColor clearColor]];
	
	randLabel = [[UILabel alloc] initWithFrame:randFrame];
		[randLabel setNumberOfLines:1];
		randLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
		[randLabel setText:[self randomString]];
		[randLabel setBackgroundColor:[UIColor clearColor]];
		randLabel.textColor = [UIColor grayColor];
		randLabel.textAlignment = NSTextAlignmentCenter;
	
	[self addSubview:headerView];
//	[headerView addSubview:EmberPlus_LogoButton];
	[headerView addSubview:EmberPlusHeaderSubLabel];
	[headerView addSubview:EmberPlusHeaderLabel];
	[headerView addSubview:randLabel];
	[headerView addSubview:EmberPlus_TwitterButton];
	[headerView addSubview:EmberPlus_PaypalButton];
	
    }
    
	return self;
}

int randNum = 0;
-(NSString*)randomString {
	//int randNum = arc4random_uniform(10);
	if (randNum == 10) randNum = 0;
	switch (randNum) {
		case 0:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			return @"Contribute. Purchase a license.";
		case 1:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			return @"How many can you swipe?";
		case 2:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			return @"By: Suhaib Alfaqeeh.";
		case 3:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			return @"Why do these labels still change?";
		case 4:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			return @"For Tinder";
		case 5:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			return @"Follow @alfadesignsco on Twitter.";
		case 6:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			//return @" test banner";
			return @"Your Card Trackin' Tinder Monitor";
		case 7:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			return @"A tinder add-on.";
		case 8:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			return @"How many can you swipe?";
		case 9:
			if (randNum == 0) randNum++;
			else if (randNum < 10 && randNum != 0) randNum++;
			
			return @"Need help? Tweet me!";
		default:
			return @"Like this tweak? Share it. Tap icon above!";
	}
}

-(UIColor*)colorWithHexString:(NSString*)hex  
{  
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];  

    // String should be 6 or 8 characters  
    if ([cString length] < 6) return [UIColor grayColor];  

    // strip 0X if it appears  
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];  

    if ([cString length] != 6) return  [UIColor grayColor];  

    // Separate into r, g, b substrings  
    NSRange range;  
    range.location = 0;  
    range.length = 2;  
    NSString *rString = [cString substringWithRange:range];  

    range.location = 2;  
    NSString *gString = [cString substringWithRange:range];  

    range.location = 4;  
    NSString *bString = [cString substringWithRange:range];  

    // Scan values  
    unsigned int r, g, b;  
    [[NSScanner scannerWithString:rString] scanHexInt:&r];  
    [[NSScanner scannerWithString:gString] scanHexInt:&g];  
    [[NSScanner scannerWithString:bString] scanHexInt:&b];  

    return [UIColor colorWithRed:((float) r / 255.0f)  
			   green:((float) g / 255.0f)  
			    blue:((float) b / 255.0f)  
			   alpha:1.0f];  
}

- (void)layoutSubviews {

    [self setSeparatorColor:[UIColor clearColor]];
    [super layoutSubviews];
}


- (void)fL_Twitter
{
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tweetbot:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tweetbot:///user_profile/alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitterrific:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitterrific:///profile?screen_name=alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=alfadesignsco"]];
	} else {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com/alfadesignsco"]];
	}
}


-(void)respring {
	system("killall -9 SpringBoard");
}

-(void)tindah {
	system("open com.cardify.tinder");
}




-(void)twitIt {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter.com/alfadesignsco"]];
}

- (void)fL_Paypal
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6BNJTR4JLFSE2"]];
}

@end