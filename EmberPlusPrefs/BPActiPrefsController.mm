#import "Preferences.h"

@implementation BPActiPrefsController

- (NSArray *)specifiers {
	if (!_specifiers) {
		NSString *compatibleName = @"BPActiPrefs";
		_specifiers = [[self loadSpecifiersFromPlistName:compatibleName target:self] retain];
	}

	return _specifiers;
}

- (void)loadView {
    [super loadView];
    [UISwitch appearanceWhenContainedIn:self.class, nil].onTintColor = [UIColor colorWithRed:0 green:0.769 blue:0.8 alpha:1]; /*#00c4cc*/
}


- (void)viewWillAppear:(BOOL)animated {
	self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0 green:0.769 blue:0.8 alpha:1]};
    self.view.tintColor = [UIColor colorWithRed:0 green:0.769 blue:0.8 alpha:1]; /*#00c4cc*/
	
    [UISwitch appearanceWhenContainedIn:self.class, nil].onTintColor = [UIColor colorWithRed:0 green:0.769 blue:0.8 alpha:1]; /*#00c4cc*/
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:0.769 blue:0.8 alpha:1]; /*#00c4cc*/ 		
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    [super viewWillAppear:animated];
	
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    
    self.view.tintColor = nil;
    self.navigationController.navigationBar.tintColor = nil;
    self.navigationController.navigationBar.titleTextAttributes = @{};
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

}
@end

@interface EmberPlus_HeaderTwo : PSTableCell
{
    UIView *headerView;
    UILabel *EmberPlusHeaderLabel;
    UILabel *EmberPlusHeaderSubLabel;
/*     UIButton *fL_TwitterButton;
    UIButton *fL_PaypalButton; */
		@public
	UILabel *randLabel;
}
@end


@implementation EmberPlus_HeaderTwo



- (void)setFrame:(struct CGRect)arg1 {
    
    [super setFrame:CGRectMake(0, 20, self.frame.size.width, 130)];
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	if((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])){
	int width = [[UIScreen mainScreen] bounds].size.width;

	CGRect randFrame = CGRectMake(0, 35, width, 60);
	self.backgroundColor = [UIColor clearColor];
	
	headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 130)];
	
	EmberPlusHeaderLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 65)] autorelease];
	[EmberPlusHeaderLabel setText:@"facebook"];
	[EmberPlusHeaderLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:55]];
	[EmberPlusHeaderLabel setTextAlignment:NSTextAlignmentCenter];
	[EmberPlusHeaderLabel setTextColor:[UIColor colorWithRed:0 green:0.769 blue:0.8 alpha:1]]; /*#3498db*/
	[EmberPlusHeaderLabel setBackgroundColor:[UIColor clearColor]];
	
	EmberPlusHeaderSubLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 70, self.frame.size.width, 25)] autorelease];
	[EmberPlusHeaderSubLabel setText:@"Issues? Try disabling some switches below."];
	[EmberPlusHeaderSubLabel setFont:[UIFont systemFontOfSize:14]];
	[EmberPlusHeaderSubLabel setTextAlignment:NSTextAlignmentCenter];
	[EmberPlusHeaderSubLabel setTextColor:[UIColor grayColor]];
	[EmberPlusHeaderSubLabel setBackgroundColor:[UIColor clearColor]];

/*	fL_TwitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[fL_TwitterButton setFrame:CGRectMake(0, 90, self.frame.size.width/2, 40)];
	[fL_TwitterButton setImage:[UIImage imageNamed:@"twitterLogo.png" inBundle:[NSBundle bundleForClass:self.class]] forState:UIControlStateNormal];
	[fL_TwitterButton addTarget:self action:@selector(fL_Twitter) forControlEvents:UIControlEventTouchUpInside];
	[fL_TwitterButton setBackgroundColor:[UIColor clearColor]]; */

/*	fL_PaypalButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[fL_PaypalButton setFrame:CGRectMake(self.frame.size.width/2, 90, self.frame.size.width/2, 40)];
	[fL_PaypalButton setImage:[UIImage imageNamed:@"PaypalLogo.png" inBundle:[NSBundle bundleForClass:self.class]] forState:UIControlStateNormal];
	[fL_PaypalButton addTarget:self action:@selector(fL_Paypal) forControlEvents:UIControlEventTouchUpInside];
	[fL_PaypalButton setBackgroundColor:[UIColor clearColor]]; */
	
	randLabel = [[UILabel alloc] initWithFrame:randFrame];
		[randLabel setNumberOfLines:1];
		randLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
		[randLabel setText:[self randomString]];
		[randLabel setBackgroundColor:[UIColor clearColor]];
		randLabel.textColor = [UIColor grayColor];
		randLabel.textAlignment = NSTextAlignmentCenter;
	
	[self addSubview:headerView];
		[headerView addSubview:EmberPlusHeaderSubLabel];
	[headerView addSubview:EmberPlusHeaderLabel];
	[headerView addSubview:randLabel];
/*	[headerView addSubview:fL_TwitterButton];
	[headerView addSubview:fL_PaypalButton]; */
	
    }
    
	return self;
}

int randNumTwo = 0;
-(NSString*)randomString {
	//int randNumTwo = arc4random_uniform(10);
	if (randNumTwo == 10) randNumTwo = 0;
	switch (randNumTwo) {
		case 0:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			return @"Thank you for your purchase.";
		case 1:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			return @"Cover your tracks with Stealth Mode.";
		case 2:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			return @"Save battery, disable VoIP.";
		case 3:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			return @"Do these labels keep changing?";
		case 4:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			return @"Try out hidden FB Experiments.";
		case 5:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			return @"Follow @alfadesignsco on Twitter.";
		case 6:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			//return @"From Codyd51";
			return @"Send unlimited photos";
		case 7:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			return @"Prevent others from watching you type!";
		case 8:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			return @"Cutomize your Facebook experience.";
		case 9:
			if (randNumTwo == 0) randNumTwo++;
			else if (randNumTwo < 10 && randNumTwo != 0) randNumTwo++;
			//return @"Is this thing on?"; 
			return @"Is this a bug?";
		default:
			return @"Love EmberPlus? Support development.";
	}
}

- (void)layoutSubviews {

    [self setSeparatorColor:[UIColor clearColor]];
    [super layoutSubviews];
}


- (void)fL_Twitter
{
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tweetbot:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tweetbot:///user_profile/alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitterrific:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitterrific:///profile?screen_name=alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=alfadesignsco"]];
	} else {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com/alfadesignsco"]];
	}
}


-(void)closeFacebook {
	system("killall -9 Facebook");
}

-(void)openFacebook {
	system("open com.facebook.Facebook");
}

-(void)respring {
	system("killall -9 SpringBoard");
}

-(void)twitIt {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter.com/alfadesignsco"]];
}

- (void)fL_Paypal
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6BNJTR4JLFSE2"]];
}

@end