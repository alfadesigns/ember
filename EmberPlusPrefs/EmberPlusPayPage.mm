#import "Preferences.h"
#import "TOWebViewController/TOWebViewController.h"
#import "PBWebViewController/PBWebViewController.h"
#import "PBSafariActivity/PBSafariActivity.h"

//second commit test

@implementation EmberPlusPayPage
- (NSArray *)specifiers {
	if (!_specifiers) {
		//NSString *compatibleName = MODERN_IOS ? @"AboutPrefs" : @"AboutPrefs";
		NSString *compatibleName = @"EmberPlusPayPrefs";
		_specifiers = [[self loadSpecifiersFromPlistName:compatibleName target:self] retain];
	}

	return _specifiers;
}

- (void)loadView {
    [super loadView];

}

- (void)viewDidLoad
{
    [super viewDidLoad];


}

- (void)checkOutCreditCard
{

				    PBWebViewController *webViewController = [PBWebViewController alloc];

		//[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=UJEW8CHEBFZQ2"]];
// Initialize the web view controller and set it's URL
webViewController = [[PBWebViewController alloc] init];

webViewController.URL = [NSURL URLWithString:@"https://plasso.co/s/n0bfb4iMeJ"];
// These are custom UIActivity subclasses that will show up in the UIActivityViewController
// when the action button is clicked
PBSafariActivity *activity = [[PBSafariActivity alloc] init];
webViewController.applicationActivities = @[activity];

// Push it
[self.navigationController pushViewController:webViewController animated:YES];
}

- (void)checkOutPayPal
{
			    PBWebViewController *webViewController = [PBWebViewController alloc];

		//[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=UJEW8CHEBFZQ2"]];
// Initialize the web view controller and set it's URL
webViewController = [[PBWebViewController alloc] init];

webViewController.URL = [NSURL URLWithString:@"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=UJEW8CHEBFZQ2"];
// These are custom UIActivity subclasses that will show up in the UIActivityViewController
// when the action button is clicked
PBSafariActivity *activity = [[PBSafariActivity alloc] init];
webViewController.applicationActivities = @[activity];

// Push it
[self.navigationController pushViewController:webViewController animated:YES];
}
- (void)goToCurtains
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"cydia://package/org.thebigboss.curtains"]];
	}
	else{	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://bit.ly/Curtains4iOS"]]; }
}

- (void)viewWillAppear:(BOOL)animated {
	self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1]};
    self.view.tintColor = [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1]; /*#1abc9c*/
	
    [UISwitch appearanceWhenContainedIn:self.class, nil].onTintColor = [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1]; /*#1abc9c*/
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1]; /*#1abc9c*/ 		
  //  [[UIApplication sharedApplication] setStatusBarHidden:YES];

    [super viewWillAppear:animated];
	
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    
    self.view.tintColor = nil;
    self.navigationController.navigationBar.tintColor = nil;
    self.navigationController.navigationBar.titleTextAttributes = @{};
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backItem];
}
@end

@interface EmberPlus_PayHeader : PSTableCell
{
    UIView *headerView;
    UILabel *EmberPlusHeaderLabel;
    UILabel *EmberPlusHeaderSubLabel;
/*     UIButton *fL_TwitterButton;
    UIButton *fL_PaypalButton; */
		@public
	UILabel *randLabel;
}
@end


@implementation EmberPlus_PayHeader



- (void)setFrame:(struct CGRect)arg1 {
    
    [super setFrame:CGRectMake(0, 20, self.frame.size.width, 130)];
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	if((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])){
	int width = [[UIScreen mainScreen] bounds].size.width;

	CGRect randFrame = CGRectMake(0, 35, width, 60);
	self.backgroundColor = [UIColor clearColor];
	
	headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 130)];
	
	EmberPlusHeaderLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 65)] autorelease];
	[EmberPlusHeaderLabel setText:@"Order a License"];
	[EmberPlusHeaderLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:35]];
	[EmberPlusHeaderLabel setTextAlignment:NSTextAlignmentCenter];
	[EmberPlusHeaderLabel setTextColor:[UIColor colorWithRed:0.173 green:0.243 blue:0.314 alpha:1]]; /*#3498db*/
	[EmberPlusHeaderLabel setBackgroundColor:[UIColor clearColor]];
	
	EmberPlusHeaderSubLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 70, self.frame.size.width, 25)] autorelease];
	[EmberPlusHeaderSubLabel setText:@"You Will Be Redirected to a Secure Page"];
	[EmberPlusHeaderSubLabel setFont:[UIFont systemFontOfSize:14]];
	[EmberPlusHeaderSubLabel setTextAlignment:NSTextAlignmentCenter];
	[EmberPlusHeaderSubLabel setTextColor:[UIColor grayColor]];
	[EmberPlusHeaderSubLabel setBackgroundColor:[UIColor clearColor]];

/*	fL_TwitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[fL_TwitterButton setFrame:CGRectMake(0, 90, self.frame.size.width/2, 40)];
	[fL_TwitterButton setImage:[UIImage imageNamed:@"twitterLogo.png" inBundle:[NSBundle bundleForClass:self.class]] forState:UIControlStateNormal];
	[fL_TwitterButton addTarget:self action:@selector(fL_Twitter) forControlEvents:UIControlEventTouchUpInside];
	[fL_TwitterButton setBackgroundColor:[UIColor clearColor]]; */

/*	fL_PaypalButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[fL_PaypalButton setFrame:CGRectMake(self.frame.size.width/2, 90, self.frame.size.width/2, 40)];
	[fL_PaypalButton setImage:[UIImage imageNamed:@"PaypalLogo.png" inBundle:[NSBundle bundleForClass:self.class]] forState:UIControlStateNormal];
	[fL_PaypalButton addTarget:self action:@selector(fL_Paypal) forControlEvents:UIControlEventTouchUpInside];
	[fL_PaypalButton setBackgroundColor:[UIColor clearColor]]; */
	
	randLabel = [[UILabel alloc] initWithFrame:randFrame];
		[randLabel setNumberOfLines:1];
		randLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
		[randLabel setText:[self randomString]];
		[randLabel setBackgroundColor:[UIColor clearColor]];
		randLabel.textColor = [UIColor grayColor];
		randLabel.textAlignment = NSTextAlignmentCenter;
	
	[self addSubview:headerView];
		[headerView addSubview:EmberPlusHeaderSubLabel];
	[headerView addSubview:EmberPlusHeaderLabel];
	[headerView addSubview:randLabel];
/*	[headerView addSubview:fL_TwitterButton];
	[headerView addSubview:fL_PaypalButton]; */
	
    }
    
	return self;
}

int randNumThree = 0;
-(NSString*)randomString {
	//int randNumThree = arc4random_uniform(10);
	if (randNumThree == 10) randNumThree = 0;
	switch (randNumThree) {
		case 0:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"Support individual developers.";
		case 1:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"Thank You for your support.";
		case 2:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"Questions? Email me.";
		case 3:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"Tap the info button for support.";
		case 4:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"Tinder, eh?";
		case 5:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"Follow @alfadesignsco on Twitter.";
		case 6:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"Check out Curtains for WhatsApp";
		case 7:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"EmberPlus - Card Counter!";
		case 8:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"Enhance your Tinder experience.";
		case 9:
			if (randNumThree == 0) randNumThree++;
			else if (randNumThree < 10 && randNumThree != 0) randNumThree++;
			return @"Is this a bug?";
		default:
			return @"Love EmberPlus? Support development.";
	}
}

- (void)layoutSubviews {

    [self setSeparatorColor:[UIColor clearColor]];
    [super layoutSubviews];
}


- (void)fL_Twitter
{
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tweetbot:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tweetbot:///user_profile/alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitterrific:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitterrific:///profile?screen_name=alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=alfadesignsco"]];
	} else {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com/alfadesignsco"]];
	}
}



- (void)checkOutPayPal
{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=UJEW8CHEBFZQ2"]];
}





-(void)respring {
	system("killall -9 SpringBoard");
}

-(void)twitIt {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter.com/alfadesignsco"]];
}

- (void)fL_Paypal
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6BNJTR4JLFSE2"]];
}

@end