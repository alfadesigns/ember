//
//  EmberPlusPrefsListController.h
//  bluePillPrefs
//
//  Created by alfaDesigns on 25.01.2014.
//  Copyright (c) 2014 alfaDesigns. All rights reserved.
//

#import "Preferences/PSListController.h"

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
 
@protocol MFMailComposeViewControllerDelegate <NSObject>
@end

@interface MFMailComposeViewController : UINavigationController
{}

- (id)_addAttachmentData:(id)arg1 mimeType:(id)arg2 fileName:(id)arg3;
- (void)addAttachmentData:(id)arg1 mimeType:(id)arg2 fileName:(id)arg3;
- (void)setMessageBody:(id)arg1 isHTML:(BOOL)arg2;
- (void)setToRecipients:(id)arg1;
- (void)setSubject:(id)arg1;

@property(nonatomic) id <MFMailComposeViewControllerDelegate> mailComposeDelegate;

@end

@interface PSListController (EmberPlus)
-(UIView*)view;
-(UINavigationController*)navigationController;
-(void)viewWillAppear:(BOOL)animated;
-(void)viewWillDisappear:(BOOL)animated;

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion;
- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion;
-(UINavigationController*)navigationController;

-(void)loadView;
@end

@interface EmberPlusPrefsListController : PSListController <MFMailComposeViewControllerDelegate>

@end
