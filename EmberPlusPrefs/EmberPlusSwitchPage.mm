#import "Preferences.h"

@implementation EmberPlusSwitchPage

- (NSArray *)specifiers {
	if (!_specifiers) {
		//NSString *compatibleName = MODERN_IOS ? @"AboutPrefs" : @"AboutPrefs";
		NSString *compatibleName = @"EmberPlusSwitchPrefs";
		_specifiers = [[self loadSpecifiersFromPlistName:compatibleName target:self] retain];
	}

	return _specifiers;
}

- (void)loadView {
    [super loadView];
    [UISwitch appearanceWhenContainedIn:self.class, nil].onTintColor = [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1]; /*#1abc9c*/
	
	
}



- (void)checkOutCreditCard
{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://plasso.co/s/xlA0DVOJuC"]];
}

- (void)checkOutPayPal
{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=4WDA6YVQV9UH4"]];
}

- (void)goToCurtains
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"cydia://package/org.thebigboss.curtains"]];
	}
	else{	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://bit.ly/Curtains4iOS"]]; }
}
- (void)goToStreamPlay
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"cydia://package/com.alfadesigns.streamplay"]];
	}
	else{	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://bit.ly/StreamPlay4iOS"]]; }
}

- (void)viewWillAppear:(BOOL)animated {
	self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1]};
    self.view.tintColor = [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1]; /*#1abc9c*/
	
    [UISwitch appearanceWhenContainedIn:self.class, nil].onTintColor = [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1]; /*#1abc9c*/
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1]; /*#1abc9c*/ 		
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    [super viewWillAppear:animated];
	
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    
    self.view.tintColor = nil;
    self.navigationController.navigationBar.tintColor = nil;
    self.navigationController.navigationBar.titleTextAttributes = @{};
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

}
@end

@interface EmberPlusSwitch_Header : PSTableCell
{
    UIView *headerView;
    UILabel *EmberPlusHeaderLabel;
    UILabel *EmberPlusHeaderSubLabel;
/*     UIButton *fL_TwitterButton;
    UIButton *fL_PaypalButton; */
		@public
	UILabel *randLabel;
}
@end


@implementation EmberPlusSwitch_Header



- (void)setFrame:(struct CGRect)arg1 {
    
    [super setFrame:CGRectMake(0, 20, self.frame.size.width, 130)];
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	if((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])){
	int width = [[UIScreen mainScreen] bounds].size.width;

	CGRect randFrame = CGRectMake(0, 35, width, 60);
	self.backgroundColor = [UIColor clearColor];
	
	headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 130)];
	
	EmberPlusHeaderLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 65)] autorelease];
	[EmberPlusHeaderLabel setText:@"Preferences"];
	[EmberPlusHeaderLabel setFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:35]];
	[EmberPlusHeaderLabel setTextAlignment:NSTextAlignmentCenter];
	[EmberPlusHeaderLabel setTextColor:[UIColor colorWithRed:1 green:0 blue:0.4 alpha:1]]; /*#3498db*/
	[EmberPlusHeaderLabel setBackgroundColor:[UIColor clearColor]];
	
	EmberPlusHeaderSubLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 70, self.frame.size.width, 25)] autorelease];
	[EmberPlusHeaderSubLabel setText:@"Issues? Try disabling some switches below."];
	[EmberPlusHeaderSubLabel setFont:[UIFont systemFontOfSize:14]];
	[EmberPlusHeaderSubLabel setTextAlignment:NSTextAlignmentCenter];
	[EmberPlusHeaderSubLabel setTextColor:[UIColor grayColor]];
	[EmberPlusHeaderSubLabel setBackgroundColor:[UIColor clearColor]];

/*	fL_TwitterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[fL_TwitterButton setFrame:CGRectMake(0, 90, self.frame.size.width/2, 40)];
	[fL_TwitterButton setImage:[UIImage imageNamed:@"twitterLogo.png" inBundle:[NSBundle bundleForClass:self.class]] forState:UIControlStateNormal];
	[fL_TwitterButton addTarget:self action:@selector(fL_Twitter) forControlEvents:UIControlEventTouchUpInside];
	[fL_TwitterButton setBackgroundColor:[UIColor clearColor]]; */

/*	fL_PaypalButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[fL_PaypalButton setFrame:CGRectMake(self.frame.size.width/2, 90, self.frame.size.width/2, 40)];
	[fL_PaypalButton setImage:[UIImage imageNamed:@"PaypalLogo.png" inBundle:[NSBundle bundleForClass:self.class]] forState:UIControlStateNormal];
	[fL_PaypalButton addTarget:self action:@selector(fL_Paypal) forControlEvents:UIControlEventTouchUpInside];
	[fL_PaypalButton setBackgroundColor:[UIColor clearColor]]; */
	
	randLabel = [[UILabel alloc] initWithFrame:randFrame];
		[randLabel setNumberOfLines:1];
		randLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
		[randLabel setText:[self randomString]];
		[randLabel setBackgroundColor:[UIColor clearColor]];
		randLabel.textColor = [UIColor grayColor];
		randLabel.textAlignment = NSTextAlignmentCenter;
	
	[self addSubview:headerView];
		[headerView addSubview:EmberPlusHeaderSubLabel];
	[headerView addSubview:EmberPlusHeaderLabel];
	[headerView addSubview:randLabel];
/*	[headerView addSubview:fL_TwitterButton];
	[headerView addSubview:fL_PaypalButton]; */
	
    }
    
	return self;
}

int randNumEmberPlus = 0;
-(NSString*)randomString {
	//int randNumEmberPlus = arc4random_uniform(10);
	if (randNumEmberPlus == 10) randNumEmberPlus = 0;
	switch (randNumEmberPlus) {
		case 0:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"Support individual developers.";
		case 1:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"Thank You for your support.";
		case 2:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"Questions? Email me.";
		case 3:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"Tap the info button for support.";
		case 4:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"EmberPlus, eh?";
		case 5:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"Follow @alfadesignsco on Twitter.";
		case 6:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"Check out Curtains for WhatsApp";
		case 7:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"EmberPlus - For Snapchat!";
		case 8:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"A snappier snapchat.";
		case 9:
			if (randNumEmberPlus == 0) randNumEmberPlus++;
			else if (randNumEmberPlus < 10 && randNumEmberPlus != 0) randNumEmberPlus++;
			return @"Is this a bug?";
		default:
			return @"Love EmberPlus? Support development.";
	}
}

- (void)layoutSubviews {

    [self setSeparatorColor:[UIColor clearColor]];
    [super layoutSubviews];
}


- (void)fL_Twitter
{
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tweetbot:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tweetbot:///user_profile/alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitterrific:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitterrific:///profile?screen_name=alfadesignsco"]];
	} else if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter:"]]) {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?screen_name=alfadesignsco"]];
	} else {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com/alfadesignsco"]];
	}
}

- (void)checkOutCreditCard
{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://plasso.co/s/WrZnXTcdPI"]];
}

- (void)checkOutPayPal
{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://plasso.co/s/WrZnXTcdPI"]];
}



-(void)respring {
	system("killall -9 SpringBoard");
}

-(void)twitIt {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter.com/alfadesignsco"]];
}

- (void)fL_Paypal
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6BNJTR4JLFSE2"]];
}

@end