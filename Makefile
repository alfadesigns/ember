ARCHS := armv7 arm64
# export DEBUG = 1
# DEBUG = 1
export GO_EASY_ON_ME = 1
export ARCHS = armv7 arm64
export TARGET=iphone:clang:latest:6.0
THEOS_DEVICE_IP = iDEVICE_IP_GOES_HERE
THEOS_DEVICE_PORT = 22
SHARED_CFLAGS = -fobjc-arc
THEOS_PACKAGE_DIR_NAME = DEBS

# TARGET_IPHONEOS_DEPLOYMENT_VERSION := 7.0
# TARGET_IPHONEOS_DEPLOYMENT_VERSION_arm64 = 7.0
# WAEnhancer_FILES = $(wildcard *.xm) $(wildcard *.m) $(wildcard Classes/*.m) $(wildcard Vendors/*.m) $(wildcard Vendors/*/*.m)

include theos/makefiles/common.mk

TWEAK_NAME = EmberPlus
EmberPlus_CFLAGS = -fobjc-arc
EmberPlus_PRIVATE_FRAMEWORKS = Preferences
EmberPlus_FILES = EmberPlus.xm SevenSwitch.m $(wildcard DMPasscode/*.m) OBGradientView.m MPNotificationView.m AFDropdownNotification.m GUAAlertView.m CustomIOS7AlertView.m JDStatusBarNotification.m JDStatusBarStyle.m JDStatusBarView.m SKBounceAnimation.m
#CupidsArrow.xm
#EmberPlus_FILES =	GUAAlertView.m CustomIOS7AlertView.m TinderTest.xm CupidsArrow.xm $(wildcard JFMinimalNotification/*.m)  $(wildcard THPinViewController/*.m) $(wildcard UIImage+ImageEffects/*.m) JDStatusBarNotification.m JDStatusBarStyle.m JDStatusBarView.m CHAvatarView.m CHDraggableView.m CHDraggableView+Avatar.m CHDraggingCoordinator.m SKBounceAnimation.m
# NACounter.m AGPushNoteView.m
EmberPlus_FRAMEWORKS = UIKit Foundation CoreGraphics QuartzCore Accelerate Security LocalAuthentication
# These frameworks are required to get the UIAlertView to work properly
include $(THEOS_MAKE_PATH)/tweak.mk
SUBPROJECTS += EmberPlusPrefs
include $(THEOS_MAKE_PATH)/aggregate.mk
after-install::
	install.exec "killall -9 Preferences; killall -9 Tinder; killall -9 backboardd;"
	#install.exec "killall -9 Preferences; killall -9 Tinder;"

	release::
	@$(MAKE) clean DEBUG="" MAKELEVEL=0 THEOS_SCHEMA="" SCHEMA="release" GO_EASY_ON_ME=1
	@$(MAKE) DEBUG="" MAKELEVEL=0 THEOS_SCHEMA="" SCHEMA="release" GO_EASY_ON_ME=1