	#import "substrate.h"
	#import <Foundation/Foundation.h>
	#import <UIKit/UIKit.h>
	#import <CoreData/CoreData.h>
	#import "NACounter.h"
	#import "JDStatusBarNotification.h"
	#import "AFDropdownNotification.h"
	#import "DMPasscode/DMPasscode.h"
	#import "CustomIOS7AlertView.h"
	#import "PropertyFile.h"
	#import "CGRectPositioning.h"
	#import <LocalAuthentication/LocalAuthentication.h>
	/* 
	***************************************************
	// File:= EmberPlus.xm					K.I.S.S.
	//  By: Suhaib Alfaqeeh 				.,¡i|¹i¡¡i¹|i¡,.<("Keep It Simple Stupid")
	// Target: com.cardify.tinder		    `"¹li¡|¡|¡il¹"     
	//  Copyright 2011-2014, alfaDesigns - All rights reserved.
	****************************************************
*/

#import "SevenSwitch.h"
#import <QuartzCore/QuartzCore.h>

#import "MPNotificationView.h"
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )
#define TinderPng @"/Library/Application Support/TNDRSelect/EmberPlusNoTextFlameLogo.png"


//====================================
// AutoLike Code
//====================================
@interface NSObject (Tinder)
-(void)sendMessage:(id)message completion:(id)completion;
-(id)recommendationsViewController;
-(id)menuViewController;
-(id)noRecommendationsView;
-(unsigned)numberOfCardsInStack;
-(id)changedMatchesFetchedResultsController;
-(id)fetchedObjects;
-(void)likeButtonTapped:(id)sender;
-(void)passButtonTapped:(id)sender;
-(void)EmberPlusSelect;

@end




@interface TNDRUser : NSObject
{

}
@end
@interface TNDRSlidingPagedViewController : UIViewController
	{
		BOOL _presentingCameraMoments;
		UIView *_mainSlidingNavbarSnapshot;
		int _defaultIndex;
		UIButton *_forceMQTTDisconnectButton;
		int _currentMomentShareTarget;
		float _curNormalizedScrollOffsetX;
		NSArray *_viewControllerKeys;
		NSDate *_startPulseTime;
	}
	
	@end
	
	@interface TNDRMatch : NSObject
-(NSSet *)messages;
@end
	
@interface TNDRSliderCell : NSObject
@property NSNumber *minimumSpan;
@property NSNumber *maximumValue;

@end
	
@interface TNDRDiscoverSettingsViewController : UIViewController
@property TNDRSliderCell *ageSliderCell;

@end

@interface TNDRDataManager : NSObject
@end

@interface TNDRMenuViewController : UIViewController
@property UITableView *tableView;
@end

@interface TNDRMenuCell : UITableViewCell
@property UIImageView *iconImageView;
@property UILabel *infoLabel;
@property UILabel *titleLabel;
@end;


@interface _ABAddressBookAddRecord 
@end
@interface AVCameraViewController: UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
UIView *_cameraOverlay; // Declares the UIView we are going to hook
// This is known because I dumped the SnapChat app headers and found this.
}
@end
@interface TNDRChatViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate> 
{
    NSString *_chatCellIdentifier;
    NSString *_likeCellIdentifier;
    float _spacingBetweenBubbles;
    NSString *_defaultPlaceholderText;
    NSMutableDictionary *_calculatedHeightsDictionary;
    NSDateFormatter *_dateFormatter;
    UIPanGestureRecognizer *_timeRevealPanGestureRecognizer;
    struct CGPoint _panStartPoint;
    struct CGPoint _panCurPoint;
    float _curDiffX;
    struct CGRect _initialComposeViewFrame;
    struct CGRect _initialComposeViewTextFrame;
    struct CGRect _initialComposeTextViewBGFrame;
    float _calcWidthOfAreaLeftOfSend;
    NSMutableArray *_fetchedObjectChanges;
    NSTimer *_testContentSizeTimer;
    UIView *_screenshotView;
    BOOL _hadFirstResponder;
    BOOL _showsLoading;
    BOOL _readyForPresentation;
    BOOL _miniStackVisible;
    BOOL _resizingEnabled;
    BOOL _attemptingBlock;
    BOOL _reloadAfterResendingMessage;
    BOOL _shouldShowStatusBar;
    BOOL _shouldReloadForMoreMessages;
    BOOL _canOpenAvatar;
    BOOL _didPresentMomentStack;
    UIView *_avatarAndNameView;
    float _offsetY;
    UIImage *_matchImage;
    UINavigationBar *_navigationBar;
    NSFetchedResultsController *_fetchedResultsController;
    NSArray *_dataItemsArray;
    UICollectionView *_collectionView;
    UICollectionViewFlowLayout *_standardFlowLayout;
    UIImageView *_keyboardImageView;
    UIView *_noMessagesView;
    UIToolbar *_composeView;
    UIButton *_sendButton;
    UIButton *_cameraButton;
    UITapGestureRecognizer *_tapAvatarTitleGestureRecognizer;
    NSMutableArray *_insertedChatCells;
    NSMutableArray *_updatedChatCells;
    unsigned int _totalCountMessages;
    int _offsetIntoMessages;
    unsigned int _currentNumMessagesToRetrieve;
    UIView *_loadingHeader;
    UIImageView *_loadingFlame;
    NSTimer *_reloadTimer;
    UIAlertView *_hideAlertView;
    UIAlertView *_followAlertView;
    UITapGestureRecognizer *_miniMomentStackTapGestureRecognizer;
    NSFetchedResultsController *_momentsFetchedResultsController;
    UIView *_chatViewScreenshotWithoutStack;
    struct CGRect _keyboardFrame;
}

@end
@protocol TNDRUpdateManagerDelegate;
@protocol THPinViewController;


	
@class NSDictionary, NSString, NSTimer, TNDRDataManager, TNDRSlidingPagedViewController, UIViewController, UIWindow;

@interface TNDRAppDelegate : _ABAddressBookAddRecord <THPinViewController, TNDRUpdateManagerDelegate, UIApplicationDelegate, NSFetchedResultsControllerDelegate>
{
    BOOL _shouldShowWhatsNew;
    NSString *_saveCurrentChatID;
    BOOL _loggingOn;
    UIViewController *_viewController;
    UIWindow *_window;
    NSTimer *_timer;
    TNDRDataManager *_dataManager;
    id _backgroundCompletionHandler;
    TNDRSlidingPagedViewController *_slidingPagedViewController;
    id <UIViewControllerTransitioningDelegate> _whatsNewTransitioningDelegate;
   
	}
	
	@end

@interface TNDRRecommendationViewController : UIViewController {
NSFetchedResultsController *_fetchedResultsController;
NSArray *_fetchedObjects;
UICollectionView *_collectionView;
TNDRRecommendationViewController *_recommendationsViewController;
// AFDropdownNotification *_notification;

}

@end

@interface TNDRTableFetchedDataSource {
NSFetchedResultsController *_fetchedResultsController;
NSArray *_fetchedObjects;
UICollectionView *_collectionView;
// AFDropdownNotification *_notification;

}
@end
@interface TNDRUserCardCellManager {
NSFetchedResultsController *_fetchedResultsController;
NSArray *_fetchedObjects;
UICollectionView *_collectionView;
// AFDropdownNotification *_notification;

}
@end


@interface TNDRPhotoPageController : UIView {
NSArray *_photoData;
}
@end	
