//
//  DecFile.h
//  EmberPlus
//
//  Created by Suhaib A.
//
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import <Social/Social.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AudioToolbox/AudioToolbox.h>
#import <notify.h>

#import <UIKit/UIAlertView.h>
OBJC_EXTERN CFStringRef MGCopyAnswer(CFStringRef key) WEAK_IMPORT_ATTRIBUTE;

#define PREFERENCES_PATH @"/var/mobile/Library/Preferences/com.alfadesigns.EmberPlusPrefs.plist"
#define PREFERENCES_CHANGED_NOTIFICATION "com.alfadesigns.EmberPlusPrefs.preferences-changed"

#define PREFERENCES_ENABLED_EmberIsTinder_KEY @"isTinder"
#define PREFERENCES_ENABLED_YTIgnition_KEY @"Ignition"
#define PREFERENCES_ENABLED_EmberPlusAuto_KEY @"Autolike"
#define PREFERENCES_ENABLED_EmberPlusCountCards_KEY @"Autolike"
#define PREFERENCES_ENABLED_EmberPlusLimitless_KEY @"Limitless"


static BOOL isTinder = NO;
static BOOL Ignition = NO;
static BOOL Autolike = NO;
static BOOL CountCards = NO;
static BOOL Limitless = NO;
