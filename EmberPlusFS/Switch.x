#import "FSSwitchDataSource.h" //Necessary for all flipswitches
#import "FSSwitchPanel.h" //Necessary for all flipswitches
#import "notify.h" //So, we can let the device know, we changed a setting
#include <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#define EmberPlusPlist @"/var/mobile/Library/Preferences/com.alfadesigns.EmberPlusPrefs.plist" 
//So we can easily reference this file, let's make it a variable
// com.alfadesigns.EmberPlus_1.1-28_iphoneos-arm 
// DEB submitted to repo. 

@interface UIAlertView (EmberPlusFSSwitch)

- (id)initWithTitle:(id)arg1 message:(id)arg2 delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5;

@end

@interface CustomIOS7AlertView (EmberPlusFSSwitch)

- (id)initWithTitle:(id)arg1 message:(id)arg2 delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5;

@end

@interface EmberPlusFSSwitch : NSObject <FSSwitchDataSource>
@end
//Necessary to delegate FSSwitchDataSource

@implementation EmberPlusFSSwitch //Where all the magic happens
-(FSSwitchState)stateForSwitchIdentifier:(NSString *)switchIdentifier { //This is where we set the state of the flipswitch (on or off)
	NSDictionary* EmberPlusSettings = [NSDictionary dictionaryWithContentsOfFile:EmberPlusPlist]; //Lets read the read receipt file
	return [[EmberPlusSettings objectForKey:@"Ignition"] boolValue] ? FSSwitchStateOn : FSSwitchStateOff; //If read receipts are on, the switch is on and vise versa
}

- (void)selectedItemAtIndex:(NSInteger)index {
    NSLog(@"Item selected at index %ld.", (long)index);
}

// - (void)EmberPlusClosed {
    
    // NSString *_content = @"EmberPlus Enabled: \n Any instance of WhatsApp has been closed. \n \v1.1-8";
    
    // UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"EmberPlus:" message:_content delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Laucnh App", nil];
    // [_moreInfo show];
    
// }

// - (void)EmberPlusOpen {
    
    // NSString *_content = @"EmberPlus Disabled: \n Any instance of WhatsApp has been closed. \n \n v1.1-8";
    
    // UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"EmberPlus:" message:_content delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Launch App", nil];
    // [_moreInfo show];
    
// }

- (void)EmberPlusClosed {
    
    NSString *_content = @"EmberPlus Enabled: \n Any instance of Tinder has been closed. \n Please restart to view changes. \n v1.1";
       UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"EmberPlus:" message:_content delegate:self cancelButtonTitle:@"Clear" otherButtonTitles:nil];
    [_moreInfo show];
    
}

- (void)EmberPlusOpen {
    
     NSString *_content = @"EmberPlus Disabled: \n Any instance of Tinder has been closed. \n v1.1";
     UIAlertView *_moreInfo = [[UIAlertView alloc] initWithTitle:@"EmberPlus:" message:_content delegate:self cancelButtonTitle:@"Clear" otherButtonTitles:nil];
     [_moreInfo show];
	
	
	//Begin Custom Alert View
	   //     UIImageView *iv = [[UIImageView alloc] initWithImage:nil];
		//[iv setImage:[UIImage imageNamed:@"ghost"]];
   //     CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
      //  [alertView setContainerView:iv];
     //   [alertView show];
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{

    if (buttonIndex == 1)
    {
	system("open net.whatsapp.WhatsApp");
    }else{
	[alertView dismiss];
	}
	}
	
	


-(void)applyState:(FSSwitchState)newState forSwitchIdentifier:(NSString *)switchIdentifier { //Let's change read receipts, based on if the user switched the switch
	NSMutableDictionary* EmberPlusSettings = [NSMutableDictionary dictionaryWithContentsOfFile:EmberPlusPlist]; //Let's get the files contents

	if(newState == FSSwitchStateIndeterminate) { //If switch is neither on or off (the user dun goofed)..
		return; //Don't do anything
	} else if(newState == FSSwitchStateOn) { //If toggled on..
		[EmberPlusSettings setObject:[NSNumber numberWithBool:YES] forKey:@"Ignition"]; //turn read reciepts on!
			[self EmberPlusClosed]; //Curtens Enabled Message

	} else if(newState == FSSwitchStateOff) { //But if toggled off..
		[EmberPlusSettings setObject:[NSNumber numberWithBool:NO] forKey:@"Ignition"]; //turn read reciepts off!
		[self EmberPlusOpen]; //EmberPlus Disabled Message

	}
	[EmberPlusSettings writeToFile:EmberPlusPlist atomically:YES]; //let's change the actual file with the changes we made above
	notify_post("com.alfadesigns.EmberPlusPrefs-prefsChanged"); //aye apple, we just changed yo shit!
	
	//Close Settings
	NSLog(@"EmberPlus: Killing: Settings.app");
    system("killall -9 Preferences"); 
	//Close Settings
	 NSLog(@"EmberPlus: Killing Tinder");
    system("killall Tinder"); 
	//Close Tinder
	// NSLog(@"EmberPlus: Re-Opening Tinder");
	// system("open net.Tinder.Tinder"); 
	//Reopen Tinder


	}

-(NSString *)titleForSwitchIdentifier:(NSString *)switchIdentifier { //the title that shows when you click the flipswitch
	return @"EmberPlus"; //ya bish
}
@end