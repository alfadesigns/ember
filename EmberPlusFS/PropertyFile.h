//
//  DecFile.h
//  EmberPlus
//
//  Created by Suhaib A.
//
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import <Social/Social.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AudioToolbox/AudioToolbox.h>
#import <notify.h>

#import <UIKit/UIAlertView.h>
OBJC_EXTERN CFStringRef MGCopyAnswer(CFStringRef key) WEAK_IMPORT_ATTRIBUTE;

#define PREFERENCES_PATH @"/var/mobile/Library/Preferences/com.alfadesigns.EmberPlusPrefs.plist"
#define PREFERENCES_CHANGED_NOTIFICATION "com.alfadesigns.EmberPlusPrefs.preferences-changed"

#define PREFERENCES_ENABLED_SKPIgnition_KEY @"Ignition"

#define PREFERENCES_ENABLED_WPNoReadReceipts_KEY @"WPNoReadReceipts"
#define PREFERENCES_ENABLED_WPHidePresence_KEY @"WPHidePresence"
#define PREFERENCES_ENABLED_WPHidePlayed_KEY @"WPHidePlayed"


static BOOL Ignition = NO;

static BOOL WPNoReadReceipts = NO;
static BOOL WPHidePresence = NO;
static BOOL WPHidePlayed = NO;