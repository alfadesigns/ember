#!/bin/bash
echo "Creating theos symlink"
ln -s /var/theos ./theos
ln -s /var/theos ./FBPPUAEnableStealthModeFS/theos
ln -s /var/theos ./EmberPlusPrefs/theos
ln -s /var/theos ./EmberPlusFS/theos
ln -s /var/theos ./BPActivateFS/theos

echo "Done."