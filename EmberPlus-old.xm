#import "PropertyFile.h"
#import "substrate.h"

/* ***************************************************
// File:= EmberPlus.xm
//  By: Suhaib Alfaqeeh
//  Copyright 2011-2014, alfaDesigns - All rights reserved.
/****************************************************
WPNoReadReceipts
WPHidePresence
WPHidePlayed

No Read Receipt
No Play Receipt
No Typing Status
No Online Status
 
 /****************************************************
// Feature: WPNoReadReceipts
//  WPNoReadReceipts = [preferences[PREFERENCES_ENABLED_WPNoReadReceipts_KEY] boolValue];
/****************************************************/
static BOOL otherRepo;


%hook ChatViewController

%group sendReadReceiptsIfNeeded
-(void)sendReadReceiptsIfNeeded { %log; if(Ignition && WPNoReadReceipts){ NSLog(@"Doing Nothing in sendReadReceiptsIfNeeded");}
else{ %orig; }}
%end
%end //END sendReadReceiptsIfNeeded

%hook XMPPConnection
%group sendReadReceiptsForChatMessages
-(void)sendReadReceiptsForChatMessages:(id)arg1 { %log; if(Ignition && WPNoReadReceipts){ NSLog(@"Doing Nothing in sendPlayedReceiptForMessage"); %orig(NULL); }
else{ %orig(arg1); }}
%end
 /****************************************************
// Feature: WPHidePresence
//  WPHidePresence = [preferences[PREFERENCES_ENABLED_WPHidePresence_KEY] boolValue];
/****************************************************/
%group sendPresenceWithNickname
-(void)sendPresenceWithNickname:(id)arg1 { %log; if(Ignition && WPHidePresence){ NSLog(@"Doing Nothing in sendPresenceWithNickname"); }
else{ %orig(arg1); }}
%end

%group createPresenceWithType
-(id)createPresenceWithType:(unsigned int)arg1 nickname:(id)arg2 { %log; if(Ignition && WPHidePresence){ NSLog(@"Doing Nothing in sendPresenceWithNickname"); return NULL;}
else{ %orig(arg1, arg2); }}
%end
 /****************************************************
// Feature: WPHidePlayed
//  WPHidePlayed = [preferences[PREFERENCES_ENABLED_WPHidePlayed_KEY] boolValue];
/****************************************************/
%group sendPlayedReceiptForMessage
-(void)sendPlayedReceiptForMessage:(id)arg1 { %log; if(Ignition && WPHidePlayed){ NSLog(@"Doing Nothing in sendPlayedReceiptForMessage");}
else{ %orig(arg1); }}
%end //END HOOK sendPlayedReceiptForMessage

%end //END Hook XMPPConnection
/*
%hook SpringBoard
-(void)applicationDidFinishLaunching:(id)application{
  %orig;
    BOOL ranBefore = [[NSUserDefaults standardUserDefaults] boolForKey:@"RanBefore"];

    if (!ranBefore) {
        UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"EmberPlus"
                          message:@"Hey there! Thank you for downloading EmberPlus. \n If you appreciate my work, please donate by using the button in the settings app."
                          delegate:self
                          cancelButtonTitle:@"Thanks!"
                          otherButtonTitles:nil];
        [alert show];
        [alert release];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"RanBefore"];
        [[NSUserDefaults standardUserDefaults] synchronize];
}
  if (otherRepo) {
    UIAlertView* repoAlert = [[UIAlertView alloc] initWithTitle:@"Hey there!" message:@"I see you've downloaded my tweak from a different repo. Please download this tweak from BigBoss to help support me and you also get faster updates!" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
    [repoAlert show];
    %orig;
  }
}
%end
*/

static void PreferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) { // Action when listener detects a change
    //Perform action when switching a preference specifier
	
    NSLog(@"Killing WhatsApp");
    system("killall WhatsApp"); //Close Skype
    NSLog(@"Killing Settings.app");
    system("killall Settings"); //Close Settings

    
 NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //ceate pointer to peferences path
 
 Ignition = [preferences[PREFERENCES_ENABLED_SKPIgnition_KEY] boolValue];// This switch defines Ignition as the preference boolean
 //Ignition points to the Activate switch in the settings bundle

    WPHidePlayed = [preferences[PREFERENCES_ENABLED_WPHidePlayed_KEY] boolValue];//Sample Switch Defeniton on initialization
    WPHidePresence = [preferences[PREFERENCES_ENABLED_WPHidePresence_KEY] boolValue];//Sample Switch Defeniton on initialization
    WPNoReadReceipts = [preferences[PREFERENCES_ENABLED_WPNoReadReceipts_KEY] boolValue];//Sample Switch Defeniton on initialization

  
}

%ctor { // runs first. This is what happens once the target (net.whatsapp.WhatsApp) is run. Prior to code-insertion. 

        if (![[NSFileManager defaultManager] fileExistsAtPath:@"/var/lib/dpkg/info/org.thebigboss.EmberPlus.list"]){
      otherRepo = YES;
    }
	
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *block) { //add listener

	NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //Define preference path here. (PREFERENCES_PATH).
	
	if (preferences == nil) {
	    preferences = @{ 
		    //PREFERENCES_ENABLED_SKPFeatureOne_KEY : @(NO), Don't forget commmas 
		    PREFERENCES_ENABLED_WPHidePlayed_KEY : @(NO),
		    PREFERENCES_ENABLED_WPHidePresence_KEY : @(NO),
		    PREFERENCES_ENABLED_WPNoReadReceipts_KEY : @(NO),

		    PREFERENCES_ENABLED_SKPIgnition_KEY : @(NO)  // No comma

			
			
			}; // Default preference value to no
	    
	    [preferences writeToFile:PREFERENCES_PATH atomically:YES];
	} else {
 WPHidePlayed = YES;	
 WPHidePresence = YES;	
 WPNoReadReceipts = YES;	
 
 Ignition = YES;
 
 WPHidePlayed = [preferences[PREFERENCES_ENABLED_WPHidePlayed_KEY] boolValue];
 WPHidePresence = [preferences[PREFERENCES_ENABLED_WPHidePresence_KEY] boolValue];
 WPNoReadReceipts = [preferences[PREFERENCES_ENABLED_WPNoReadReceipts_KEY] boolValue];
 
 Ignition = [preferences[PREFERENCES_ENABLED_SKPIgnition_KEY] boolValue];
	    
		
			if(Ignition){ 
	NSLog(@"EmberPlus: EmberPlus Activated: Initializing");
	if(WPHidePlayed){NSLog(@"EmberPlus: PlayedHooks Activated"); %init(sendPlayedReceiptForMessage);  }else{NSLog(@"EmberPlus: PlayedHooks Deactivated"); }
	if(WPHidePresence){NSLog(@"EmberPlus: PresenceHooks Activated"); %init(sendPresenceWithNickname); %init(createPresenceWithType); } else{NSLog(@"EmberPlus: PresenceHooks Deactivated"); }
	if(WPNoReadReceipts){NSLog(@"EmberPlus: ReadHooks Activated"); %init(sendReadReceiptsIfNeeded); %init(sendReadReceiptsForChatMessages); }else{NSLog(@"EmberPlus: ReadHooks Deactivated"); }

	}
	else{
	NSLog(@"EmberPlus: EmberPlus Deactivated. Target will run unmodified.");
	}
	
	}
	CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, PreferencesChangedCallback, CFSTR(PREFERENCES_CHANGED_NOTIFICATION), NULL, CFNotificationSuspensionBehaviorCoalesce);
    }];
}