#import "EmberPlus.h"
static TNDRDataManager *_hookedDataManager;
static TNDRRecommendationViewController *_hookedRVC;
static TNDRSlidingPagedViewController *_hookedSlidingPagedViewController;
static TNDRSlidingPagedViewController *_secondSPV;
static TNDRChatViewController *_hookedTNDRChatViewController;
static TNDRUser *_hookedTNDRUser;
static NSFetchedResultsController *_hookedFetchedResultsController; // our hooked NSFetchedResultsController object. 
static UICollectionView *_hookedCollectionView; // our hooked UICollectionView object. 


//======================
// Keep it simple stupid
//=====================	

@interface UIViewController (startTimer) 
@end

NSInteger rowsInTable;
//NSInteger swipeInSeconds = 2; //Default interval is 2 seconds between each swipe.
 NSString *settingsPlistPath = @"/var/mobile/Library/Preferences/com.alfadesigns.EmberPlusPrefs.plist";

 NSMutableDictionary *settingsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:settingsPlistPath];
 int swipeInSeconds = [[settingsDictionary objectForKey:@"Interval"] intValue];


@implementation UIViewController (startTimer)
	

static int numLeft = 0;
-(void)tapLikeButton:(id)sender {
	UIViewController *controller = [self recommendationsViewController];
	float tallyFloat = [controller numberOfCardsInStack];
	float progress = 0;
	float ratio = (progress / tallyFloat);
	if([controller numberOfCardsInStack] > 0 &&  [(UISwitch *)[[controller view] viewWithTag:666678] isOn]) {
		// You can add more if cases here
		if(Autolike){
			[controller likeButtonTapped:nil];
		}
		else{
			[controller passButtonTapped:nil];
		}
		
		if(CountCards){
			[MPNotificationView notifyWithText:@"EmberPlus:"
detail:[NSString stringWithFormat:@"You've got %d card(s) remaining to swipe.", [controller numberOfCardsInStack]]
image:[UIImage imageWithContentsOfFile:@"/Library/Application Support/TNDRSelect/EmberPlusNoTextFlameLogo.png"]
andDuration:1.0]; 
		}			   
	}
	
	if([controller numberOfCardsInStack] <= 0){
		
		
		static dispatch_once_t once;
		dispatch_once(&once, ^ { 
			// Code to run once
			[MPNotificationView notifyWithText:@"EmberPlus:"
detail:[NSString stringWithFormat:@"No cards available yet. Check back soon.", [controller numberOfCardsInStack]]
image:[UIImage imageWithContentsOfFile:@"/Library/Application Support/TNDRSelect/EmberPlusNoTextFlameLogo.png"]
andDuration:1.0]; 
		});

	}

	[self startTimer];
}

-(void)startTimer {
	NSTimer *timer = [NSTimer	scheduledTimerWithTimeInterval:swipeInSeconds 
target:self 
selector:@selector(tapLikeButton:)
userInfo:nil 
repeats:NO];
	[[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

@end
%hook TNDRRecommendationViewController

- (void)viewDidLoad {
	#ifdef DEBUG 
	%log; 
	#endif
	%orig;
	
	
	CGRect viewFrame = [((UIView *)[self view]) frame];    
	
	UIImage *btnImage = [UIImage imageWithContentsOfFile:@"/Library/Application Support/TNDRSelect/EmberPlusNoTextFlameLogo.png"];
	UIImage *btnImageHighlighted = [UIImage imageWithContentsOfFile:@"/Library/Application Support/TNDRSelect/EmberPlusNoTextFlameLogo.png"];
	int btnHeight = btnImage.size.height; // Get the dimensions of the image to make the button the correct size.
	int btnWidth = btnImage.size.width;
	UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
	btn.frame = CGRectMake(215.5, IS_IPHONE_5 ? 515.5 : 427.5 , btnWidth, btnHeight); // Comparison to get the correct height
	[btn setImage:btnImage forState:UIControlStateNormal];
	[btn setImage:btnImageHighlighted forState:UIControlStateHighlighted];
	[btn addTarget:self action:@selector(EmberPlusSelect) forControlEvents:UIControlEventTouchUpInside];
	//	[btn addTarget:self action:@selector(EmberPlusSelect) forControlEvents:UIControlEventTouchUpInside];

	//UISwitch *mySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0,0,0,0)];
	SevenSwitch *mySwitch = [[SevenSwitch alloc] initWithFrame:CGRectMake(0,0,0,0)];
	mySwitch.frame = CGRectMake((viewFrame.size.width - mySwitch.frame.size.width)/2.0, 386.5f, mySwitch.frame.size.width, mySwitch.frame.size.height);

	//mySwitch.frame = CGRectMake((viewFrame.size.width - mySwitch.frame.size.width)/2.0, (viewFrame.size.height - mySwitch.frame.size.height)/2.0 - 50.0f, mySwitch.frame.size.width, mySwitch.frame.size.height);
	mySwitch.tag = 666678;
	mySwitch.alpha = 0.5f;
	//mySwitch.center = CGPointMake(self.view.bounds.size.width * 0.5, self.view.bounds.size.height * 0.5 - 80);
	//mySwitch.offImage =  [UIImage imageWithContentsOfFile:@"/Library/Application Support/TNDRSelect/EmberPlusNoTextFlameLogo.png"];
	mySwitch.onImage =  [UIImage imageWithContentsOfFile:@"/Library/Application Support/TNDRSelect/EmberPlusNoTextFlameLogo.png"];
	mySwitch.tintColor = [UIColor colorWithHue:0.08f saturation:0.74f brightness:1.00f alpha:1.00f];
	//  mySwitch.isRounded = NO;
	//[btn setThumbTintColor:[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/TNDRSelect/EmberPlusNoTextFlameLogo.png"]]];

	//mySwitch.thumbTintColor = [UIColor colorWithRed:0.19f green:0.23f blue:0.33f alpha:1.00f];
	mySwitch.activeColor = [UIColor colorWithRed:0.784 green:0.784 blue:0.784 alpha:1];
	mySwitch.inactiveColor = [UIColor colorWithRed:0.07f green:0.09f blue:0.11f alpha:1.00f];
	mySwitch.tintColor = [UIColor colorWithRed:0.45f green:0.58f blue:0.67f alpha:1.00f];
	mySwitch.borderColor = [UIColor clearColor];
	mySwitch.onColor = [UIColor colorWithRed:1 green:0 blue:0.4 alpha:1];
	%log(@"CupidsLog: Creating mySwitch"); 
	//[mySwitch onTintColor:[UIColor colorWithRed:0.784 green:0.784 blue:0.784 alpha:1]];
	[mySwitch addTarget:self action:nil forControlEvents:UIControlEventValueChanged];
	// See if you can implement forControlEvents:UITouchDown for button push. 
	// Feature idea: "Hold" to autolike?
	// See if perhaps the swipe timer can be lowered even further. Test at a float value of 0.5 first. Should be noticable enough.

	[(UIView*)[self view] addSubview:mySwitch];
	%log(@"CupidsLog: mySwitch Created"); 
	//[(UIView*)[self view] addSubview:btn];
}


%new
-(void)EmberPlusSelect {
	%log(@"CupidsLog: Oi"); } 
%end


%hook TNDRDiscoverSettingsViewController
- (id)ageSliderCell {
TNDRSliderCell *ageSliderCell = %orig;	
//ageSliderCell.minimumSpan = [NSNumber numberWithInt:1];
ageSliderCell.minimumSpan = [NSNumber numberWithInt:[[settingsDictionary objectForKey:@"ageSpan"] intValue]];

return ageSliderCell;
}
/* - (id)distanceSliderCell {
TNDRSliderCell *distanceSliderCell = %orig;	
//ageSliderCell.minimumSpan = [NSNumber numberWithInt:1];
distanceSliderCell.maximumValue = [NSNumber numberWithInt:[[settingsDictionary objectForKey:@"increaseRadius"] intValue]];

return distanceSliderCell;
} */

%end
	
%hook TNDRMenuViewController
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	rowsInTable = %orig;
	rowsInTable++;
	return rowsInTable;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat height = %orig;
	return height - 13;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row < rowsInTable - 1) {
		%orig;
	} else {
		[tableView deselectRowAtIndexPath:indexPath animated:YES];
	}
}

- (TNDRMenuCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row != rowsInTable - 1) {
		return %orig;
	} else {
		NSIndexPath *indPth = [NSIndexPath indexPathForRow:indexPath.row - 1 inSection:indexPath.section];
		TNDRMenuCell *cell = %orig(tableView, indPth);
		cell.iconImageView.image = nil;
		UISegmentedControl *segCtrl = [[UISegmentedControl alloc] initWithItems:@[@"\U000025BC\U0000FE0E" , @"\U000025B2\U0000FE0E"]];
		segCtrl.tintColor = [UIColor lightGrayColor];
		segCtrl.frame = CGRectMake(8, 12, 42, 20);
		[segCtrl addTarget:self action:@selector(segCtrlValueChanged:) forControlEvents:UIControlEventValueChanged];
		[cell.contentView addSubview:segCtrl];
		if(Autolike){
			cell.infoLabel.text = [NSString stringWithFormat:@"Swipe Direction: Right"];
		}
		else{		cell.infoLabel.text = [NSString stringWithFormat:@"Swipe Direction: Left"];

		}
		cell.titleLabel.text = [NSString stringWithFormat:@"Auto-Swipe Interval: %d sec",  (int)swipeInSeconds];
		return cell;
	}
}

%new

//Loops counter back from 9 to 1. (Cyclical Counter)
-(void)segCtrlValueChanged:(UISegmentedControl *)sender {
	NSInteger decOrInc = sender.selectedSegmentIndex;
	sender.selectedSegmentIndex = -1;
	swipeInSeconds = decOrInc == 0 ? swipeInSeconds - 1 : swipeInSeconds + 1;
	if (swipeInSeconds < 1) {
		swipeInSeconds = 10;
	}
	if (swipeInSeconds > 10) {
		swipeInSeconds = 1;
	}
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rowsInTable - 1 inSection:0];
	TNDRMenuCell *cell = (TNDRMenuCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	[cell.titleLabel setText:[NSString stringWithFormat:@"Auto-Swipe Every %d sec", swipeInSeconds]];
	//  [cell.infoLabel setText:[NSString stringWithFormat:@"Auto-Swipe right every %d seconds.", swipeInSeconds]];
}

%end
//End Auto Like Code
//====================================


%hook TNDRChatViewController
- (void)viewDidLoad {
	%orig; // Call the original method implementations (VERY IMPORTANT)

}
%end

%hook TNDRDataManager
-(bool)isUpdatingRecommendations { if(Ignition) {
		%log;
		bool result = %orig;
		NSLog(@"CupidLog isUpdatingMatches: %d", result);
		return result;}
	else{ return %orig; }
}

-(void)setChangedPreferences:(bool)fp8 {  %log;  if(Ignition) {
		[JDStatusBarNotification showWithStatus:@"Settings Saved"  dismissAfter:2.0 styleName:JDStatusBarStyleWarning];
		[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite];

		%orig;	} else { %orig; }
}
%end

%hook TNDRSlidingPagedViewController

- (void)viewDidLoad {
	#ifdef DEBUG 
	%log; 
	#endif
	%orig;

	[[UIApplication sharedApplication] setIdleTimerDisabled: YES];
	[self startTimer];
} 

-(void)viewWillAppear:(bool)arg1 {  if(Ignition) {
		%orig(arg1);
		[JDStatusBarNotification showWithStatus:@"EmberPlus" dismissAfter:2.0 styleName:JDStatusBarStyleError];
		[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite];
		%orig;	} else { %orig; }
}
%end



// Project: TinderTest.xm : CupidsArrow -- "Prototype"
// Class:    TNDRRecommendationViewController
// Instance Variable: NSFetchedResultsController *fetchedResultsController
// Array:      fetchedObjects
// Method:   count
//This will retrieve the number of reccomendations fetched. (i.e. The total number of profiles you have left to swipe)
TNDRRecommendationViewController *_recommendationView; // Hooked TNDRRecommendationViewController
int fetchedCount;// Where we'll store number of fetched recommendations.
int storedCount = 0; // If fetchedObjects is suddenly greater than oldCount then reccomendations list was refreshed and updated. 


/* %hook TNDRNoRecommendationsView // When you've got no more profiles left to swipe.
-(void)layoutSubviews { // Once we run out of profiles 
if(Ignition) {
	[JDStatusBarNotification showWithStatus:@"No Tinder cards yet. Check back soon." dismissAfter:2.0 styleName:JDStatusBarStyleError]; // Drop notification
	%orig;	} else { %orig; }

}
%end */

/* %hook TNDRMomentStackViewController

- (id)cardStackView:(id)fp8 cardForIndex:(int)fp12 { 
	%log;
	id result = %orig;
	NSLog(@"CupidLog: cardStackView 1st arg value %@", fp8);
	NSLog(@"CupidLog cardStackView	cardForIndex: %d", fp12);
	return result;
}
int totalStack = 0;
- (int)numberOfCardsOnStack:(id)fp8{
if(Ignition) {
	int result = %orig;
	int correctedResult = result-1;
	NSLog(@"CupidLog: numberOfCardsOnStack 1st arg value %@", fp8);
	if((totalStack !=0) && (result > totalStack)){
		[JDStatusBarNotification showWithStatus:[NSString stringWithFormat:@"List Updated: %d recommendations cards available!", correctedResult]];			
				[JDStatusBarNotification showProgress:0.0];

	}
	totalStack = %orig;
	return %orig;
}

else {
return %orig;
}
}
%end */

//%group LimitlessSwipes

%hook TNDRAdCardCellManager

-(bool)internal_isTinderPlusSubscriber {
	if(Ignition){ return TRUE; } else{ return %orig;} 
}

-(void)setInternal_isTinderPlusSubscriber:(bool)arg1 {
	if(Ignition){ bool r = TRUE; %orig(r); } else{ %orig(arg1); } }

-(void)configureForTinderPlusSubscriptionStatus:(bool)arg1 {
	if(Ignition) {
		%orig(TRUE);
	}
	else{
		%orig(arg1);
	} }
%end

%hook TNDRPaywallGatekeeper
-(bool)isPaywallBeingShownToUser {
	if(Ignition){ return FALSE; } else{ return %orig; } 
}

-(void)setPaywallBeingShownToUser:(bool)arg1 {
	if(Ignition){ bool r = FALSE; %orig(r); } else{ %orig(arg1); } }


-(bool)isTinderPlusActive { if(Ignition) {
		return TRUE; } else{ return %orig; } }

%end //=====End TNDPayWallGatekeeper hook

%hook TNDRCurrentUser
-(bool)isTinderPlusSubscriber {
	if(Ignition){ return TRUE; } else{ return %orig;} 
}

-(bool)isTinderPlusEnabled {
	if(Ignition){ return TRUE; } else{ return %orig;} 
}
%end

//%end // END GROUP Limitless Swipes
/* %hook TNDRAnalyticsTracker

-(void)trackPassportChooseLocationWithLat:(double)arg1 andLong:(double)arg2 isExpired:(bool)arg3 isTinderPlusSubscriber:(bool)arg4 {
	if(Ignition){ bool r = FALSE; bool s = TRUE; %orig(arg1, arg2, r, s); } else{ %orig(arg1, arg2, arg3, arg4); }
}

-(void)trackTinderPlusRoadblockViewWithProductInfo:(id)arg1 otherID:(id)arg2 percentLikesLeft:(double)arg3  timeRemaining:(long long)arg4 {
	if(Ignition) {
		double r = 100.0;
		long long s = 0.0;
		%orig(arg1, arg2, r, s);
	}
	else{
		%orig(arg1, arg2, arg3, arg4);
	}
}

-(void)trackTinderPlusRoadblockCancelWithProductInfo:(id)arg1 otherID:(id)arg2 percentLikesLeft:(double)arg3  timeRemaining:(long long)arg4 {
	if(Ignition) { double r = 100.0; long long s = 0.0; %orig(arg1, arg2, r, s); } else{ %orig(arg1, arg2, arg3, arg4); } }

-(void)trackTinderPlusRoadblockSelectSku:(id)arg1 withPrice:(id)arg2 andLocale:(id)arg3  otherID:(long long)arg4 percentLikesLeft:(double)arg5	timeRemaining:(long long)arg6{
	if(Ignition) { double r = 100.0; long long s = 0.0; %orig(arg1, arg2, arg3, arg4, r, s); } else{ %orig(arg1, arg2, arg3, arg4, arg5, arg6); } }

-(void)trackTinderPlusPurchaseOption:(id)arg1 withPrice:(id)arg2 andLocale:(id)arg3 percentLikesLeft:(double)arg4 unlimitedLikesOffered:(bool)arg5 {
	if(Ignition) { double r = 100.0; bool s = TRUE; %orig(arg1, arg2, arg3, r, s); } else{ %orig(arg1, arg2, arg3, arg4, arg5); } }
	
-(void)trackTinderPlusPurchase:(id)arg1 withPrice:(id)arg2 andLocale:(double)arg3  from:(unsigned long long)arg4 percentLikesLeft:(double)arg5	unlimitedLikesOffered:(bool)arg6 {
	if(Ignition) { double r = 100.0; bool s = TRUE; %orig(arg1, arg2, arg3, arg4, r, s); } else{ %orig(arg1, arg2, arg3, arg4, arg5, arg6); } }

-(void)trackTinderPlusPaywallViewAllProducts:(id)arg1 encounteredFrom:(unsigned long long)arg2 percentLikesLeft:(double)arg3 unlimitedLikesOffered:(bool)arg4 {
	if(Ignition){ double r = 100.0; bool s = TRUE; %orig(arg1, arg2, r, s); } else{ %orig(arg1, arg2, arg3, arg4); } }
	
-(void)trackTinderPlusExitPaywallCancelWithProductInfo:(id)arg1 percentLikesLeft:(double)arg2 unlimitedLikesOffered:(bool)arg3 {
if(Ignition){ double r = 100.0; bool s = TRUE; %orig(arg1, r, s); } else{ %orig(arg1, arg2, arg3); } }

%end 
	
%hook TNDRRecommendationViewController
- (void)finishSwipingInDirection:(int)fp8 withVelocity:(struct CGPoint)fp12 {
if(Ignition) {
	_hookedCollectionView = MSHookIvar<UICollectionView *>(self, "_collectionView");	//hook into _fetchedResultsController Class Instance
//	_hookedRVC = MSHookIvar<TNDRRecommendationViewController	 *>(self, "_hookedRVC");	//hook into _fetchedResultsController Class Instance
	if([_hookedCollectionView.visibleViews count] > storedCount)	{ 
		fetchedCount = [_hookedCollectionView.visibleViews count]; // Extract count of fetched results
		NSLog(@"CupidCount:  fetchedCount  = %d  ============ 1", fetchedCount); //Log the fetchedCount total
		NSLog(@"CupidCount: _hookedCollectionView %lu", [_hookedCollectionView.visibleViews count] );	
		int correctedResults = fetchedCount-1; 
		NSLog(@"CupidCount: List Updated Sucessfully to %d ", (fetchedCount-1));  
		[JDStatusBarNotification showWithStatus:[NSString stringWithFormat:@"List was updated! %d cards now available to swipe!", correctedResults] styleName:JDStatusBarStyleSuccess]; }
	else{
		NSLog(@"CupidCount: List  Not Updated: Proceeding To Queue %d ", (fetchedCount-1));  
		fetchedCount = [_hookedCollectionView.visibleViews count]; // Extract count of fetched results
		int correctedResults = fetchedCount-1; 
		//Show The Notification When Swiping and No Update has Taken Place.
		
		[JDStatusBarNotification showWithStatus:[NSString stringWithFormat:@"You've got %d recommendations remaining in queue", correctedResults]]; //Banner Notify Count
		[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite];
			[JDStatusBarNotification showProgress:1.0];

			//
//notification.image = [UIImage imageWithContentsOfFile:@"/Library/Application Support/TNDRSelect/stack.png"];

	}
	storedCount = fetchedCount;
//	[self likeUserForTopCard];

	%orig;
	}
	else { %orig; }
	} 
	
	%end
	
-(id)cellForTopCardOnStack { //Use to retrieve index for front-most user on stack. [NSArray count]
	id originalLogic = %orig; // This is the original return value the method is expected to return. We still need it to do it's job. Whatever that may be.
	_hookedFetchedResultsController = MSHookIvar<NSFetchedResultsController *>(self, "_fetchedResultsController");	//hook into _fetchedResultsController Class Instance
	return originalLogic;
}
	
	%new 
	- (UIView *)createDemoView
{

UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 200)];
UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
NSLog(@"CurtainsDemo: ");
return demoView;
}

%end	 
*/


static void PreferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) { // Action when listener detects a change
	//Perform action when switching a preference specifier
	
	NSLog(@"Killing Tinder");
	system("killall Tinder"); //Close Tinder
	NSLog(@"Killing Settings.app");
	system("killall Settings"); //Close Settings

	
	NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //ceate pointer to peferences path

	Ignition = [preferences[PREFERENCES_ENABLED_YTIgnition_KEY] boolValue];// This switch defines Ignition as the preference boolean
	//Ignition points to the Activate switch in the settings bundle



}
%ctor { // runs first. This is what happens once the target is run. Prior to code-insertion. 
	NSLog(@"CupidsArrow:  Injecting Tinder.dylib");
	
	
	[[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *block) { //add listener

		NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //Define preference path here. (PREFERENCES_PATH).
		
		if (preferences == nil) {
			preferences = @{ 

				PREFERENCES_ENABLED_EmberIsTinder_KEY : @(NO),  // comma //Don't forget to save the property file in EmberPlusPrefs directory
				PREFERENCES_ENABLED_YTIgnition_KEY : @(NO),  // comma
				PREFERENCES_ENABLED_EmberPlusCountCards_KEY : @(NO),  // comma
				PREFERENCES_ENABLED_EmberPlusLimitless_KEY : @(NO),  // comma
				PREFERENCES_ENABLED_EmberPlusAuto_KEY : @(NO)  // No comma

				
				
			}; // Default preference value to no
			
			[preferences writeToFile:PREFERENCES_PATH atomically:YES];
		} else {

			isTinder = YES; //Attach as action to pay page
			Ignition = YES;
			Autolike = YES;
			CountCards = YES;
			Limitless = YES;


			isTinder = [preferences[PREFERENCES_ENABLED_EmberIsTinder_KEY] boolValue]; // Use this later for DRM activation
			Ignition = [preferences[PREFERENCES_ENABLED_YTIgnition_KEY] boolValue];
			Autolike = [preferences[PREFERENCES_ENABLED_EmberPlusAuto_KEY] boolValue];
			CountCards = [preferences[PREFERENCES_ENABLED_EmberPlusCountCards_KEY] boolValue];
			Limitless = [preferences[PREFERENCES_ENABLED_EmberPlusLimitless_KEY] boolValue];
			
			
			if(Ignition){ 
				NSLog(@"EmberPlus: EmberPlus Activated: Initializing");
				//	%init;
			}
			else{
				NSLog(@"EmberPlus: EmberPlus Deactivated. Target will run unmodified.");
			}
			
			if(Limitless){ 
				NSLog(@"EmberPlus: Limitless Activated: Initializing");
				//%init(LimitlessSwipes);
			}
			else{
				NSLog(@"EmberPlus: Limitless Deactivated. Target will run unmodified.");
			}
			
		}
		CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, PreferencesChangedCallback, CFSTR(PREFERENCES_CHANGED_NOTIFICATION), NULL, CFNotificationSuspensionBehaviorCoalesce);
	}];
}